#pragma once

class RendererDX10 
{
private:
	ID3D10Device* d3dDevice;
	IDXGISwapChain* SwapChain;
	ID3D10RenderTargetView* RenderTargetView;

	float red = 0.0f;
	float green = 0.0f;
	float blue = 0.0f;
	int colormodr = 1;
	int colormodg = 1;
	int colormodb = 1;
public:
	bool InitD3D(HINSTANCE hInstance, HWND hWnd, int width, int height);
	bool InitScene();
	void UpdateScene();
	void DrawScene();
	void ReleaseObjects();
};

