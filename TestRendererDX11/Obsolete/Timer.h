#pragma once
#include <windows.h>
#include <string>
#include <wincodec.h>
#include <iostream>
#include <fstream>

struct Timer
{
public:
	double timerFrequency = 0.0;
	long long lastFrameTime = 0;
	long long lastSecond = 0;
	double frameDelta = 0;
	int fps = 0;

	Timer();
	double GetFrameDelta();
};

