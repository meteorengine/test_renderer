#include "stdafx.h"
#include "AdapterInfo.h"
#include "RendererType.h"
#include "VertexBufferBinding.h"
#include "CullMode.h"
#include "BlendState.h"
#include "DepthStencilState.h"
#include "DeviceState.h"
#include "PixelFormat.h"
#include "PixelFormatInfo.h"
#include "Adapter.h"
class GraphicsDevice;
#include "RenderingContext.h"
#include "GraphicsDevice.h"
#include "RenderingHelpers.h"
class GraphicsDeviceDX12;
#include "RenderingContextDX12.h"
#include "GraphicsDeviceDX12.h"


RenderingContextDX12::RenderingContextDX12(GraphicsDevice* device, HWND output, std::string name, bool main) : RenderingContext(device, output, name, main)
{
	_swapChains = std::map<HWND, SwapChainEntry>();
	_device = (GraphicsDeviceDX12*)device;
	_memoryUsage = 0;
}


RenderingContextDX12::~RenderingContextDX12()
{
	release();
	_device = NULL;
}

bool RenderingContextDX12::Resize(int width, int height, bool fullscreen) {
	if (fullscreen == IsFullscreen(), width == Width(), height == Height()) {
		return false;
	}
	if (fullscreen && !_device->Adapter()->PrimaryOutput) {
		if (width == Width() && height == Height()) {
			return false;
		}
		fullscreen = false;
	}
	Device->AddLocker();
	bool result;
	__try {
		release();
		D3D12_VIEWPORT vp;
		vp.TopLeftX = 0;
		vp.TopLeftY = 0;
		#pragma warning(push)
		#pragma warning( disable: 4244)
		vp.Width = width;
		vp.Height = height;
		#pragma warning(pop)
		vp.MinDepth = 0;
		vp.MaxDepth = 1;
		viewport = vp;

		if (output != 0) {
			DXGI_MODE_DESC backBufferDesc = {};
			backBufferDesc.Width = width;
			backBufferDesc.Height = height;
			backBufferDesc.Format = RenderingHelpers::GetDXGIFormat(Device->DefaultBackBufferFormat());
			backBufferDesc.RefreshRate.Numerator = 60;
			backBufferDesc.RefreshRate.Denominator = 1;
			backBufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
			backBufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

			DXGI_SWAP_CHAIN_DESC swapChainDesc = {};
			swapChainDesc.BufferCount = FrameCount;
			swapChainDesc.BufferDesc = backBufferDesc;
			swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
			swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
			swapChainDesc.OutputWindow = output;
			swapChainDesc.SampleDesc.Count = 1;
			swapChainDesc.SampleDesc.Quality = 0;
			swapChainDesc.Windowed = TRUE;
			swapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

			IDXGISwapChain* tempSwapChain;
			GraphicsDevice::FactoryDXGI->CreateSwapChain(_device->Queue, &swapChainDesc, &tempSwapChain);
			if (IsMain() && fullscreen) {
				tempSwapChain->ResizeTarget(&backBufferDesc);
				tempSwapChain->SetFullscreenState(true, NULL);
				tempSwapChain->ResizeBuffers(swapChainDesc.BufferCount, width, height, backBufferDesc.Format, swapChainDesc.Flags);
			}

			int textureMemoryUsage = RenderingHelpers::GetTextureMemoryUsage(backBufferDesc.Format, backBufferDesc.Width, backBufferDesc.Height, 1) * swapChainDesc.BufferCount;
			_memoryUsage += textureMemoryUsage;
			_device->PrivateDeviceMemoryUsage += textureMemoryUsage;

			swapChain = static_cast<IDXGISwapChain3*>(tempSwapChain);

			frameIndex = swapChain->GetCurrentBackBufferIndex();

			D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc = {};
			rtvHeapDesc.NumDescriptors = FrameCount;
			rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
			rtvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;

			_device->Device->CreateDescriptorHeap(&rtvHeapDesc, IID_PPV_ARGS(&rtvDescriptorHeap));

			rtvDescriptorSize = _device->Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);

			CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle(rtvDescriptorHeap->GetCPUDescriptorHandleForHeapStart());
			for (int i = 0; i < FrameCount; i++)
			{
				swapChain->GetBuffer(i, IID_PPV_ARGS(&renderTargets[i]));

				_device->Device->CreateRenderTargetView(renderTargets[i], nullptr, rtvHandle);
				D3D11_RESOURCE_FLAGS d3d11Flags = { D3D11_BIND_RENDER_TARGET };
				_device->DeviceDX11OnDX12->CreateWrappedResource(
					renderTargets[i],
					&d3d11Flags,
					D3D12_RESOURCE_STATE_RENDER_TARGET,
					D3D12_RESOURCE_STATE_PRESENT,
					IID_PPV_ARGS(&wrappedBackBuffers[i])
				);

				// Create a render target for D2D to draw directly to this back buffer.
				IDXGISurface* surface;
				wrappedBackBuffers[i]->QueryInterface<IDXGISurface>(&surface);

				D2D1_RENDER_TARGET_PROPERTIES props = D2D1::RenderTargetProperties(D2D1_RENDER_TARGET_TYPE_DEFAULT, D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED));
				_device->FactoryDirect2D1->CreateDxgiSurfaceRenderTarget(surface, &props, &renderTargets2D[i]);
				SAFE_RELEASE(surface);
				renderTargets2D[i]->SetAntialiasMode(D2D1_ANTIALIAS_MODE_PER_PRIMITIVE);
				renderTargets2D[i]->SetTextAntialiasMode(D2D1_TEXT_ANTIALIAS_MODE_CLEARTYPE);

				rtvHandle.Offset(1, rtvDescriptorSize);
			}

			for (int i = 0; i < FrameCount; i++)
			{
				_device->Device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&commandAllocator[i]));
			}

			_device->Device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, commandAllocator[0], NULL, IID_PPV_ARGS(&commandList));
			commandList->Close();

			for (int i = 0; i < FrameCount; i++)
			{
				_device->Device->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&fence[i]));
				fenceValue[i] = 0;
			}
			fenceEvent = CreateEvent(nullptr, FALSE, FALSE, nullptr);

			SwapChainEntry schain;

			for (int i = 0; i < FrameCount; i++)
			{
				schain.commandAllocator[i] = commandAllocator[i];
				schain.fence[i] = fence[i];
				schain.fenceValue[i] = fenceValue[i];
				schain.renderTargets[i] = renderTargets[i];
				schain.renderTargets2D[i] = renderTargets2D[i];
				schain.wrappedBackBuffers[i] = wrappedBackBuffers[i];

			}
			schain.commandList = commandList;
			schain.fenceEvent = fenceEvent;
			schain.rtvDescriptorHeap = rtvDescriptorHeap;
			schain.rtvDescriptorSize = rtvDescriptorSize;
			schain.frameIndex = frameIndex;
			schain.swapChain = swapChain;
			_swapChains[output] = schain;
		}

		D3D12_DESCRIPTOR_HEAP_DESC dsvHeapDesc = {};
		dsvHeapDesc.NumDescriptors = 1;
		dsvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
		dsvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
		_device->Device->CreateDescriptorHeap(&dsvHeapDesc, IID_PPV_ARGS(&dsDescriptorHeap));

		D3D12_DEPTH_STENCIL_VIEW_DESC depthStencilDesc = {};
		depthStencilDesc.Format = RenderingHelpers::GetDXGIFormat(Device->DefaultDepthBufferFormat());;
		depthStencilDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
		depthStencilDesc.Flags = D3D12_DSV_FLAG_NONE;

		D3D12_CLEAR_VALUE depthOptimizedClearValue = {};
		depthOptimizedClearValue.Format = DXGI_FORMAT_D32_FLOAT;
		depthOptimizedClearValue.DepthStencil.Depth = 1.0f;
		depthOptimizedClearValue.DepthStencil.Stencil = 0;

		_device->Device->CreateCommittedResource(
			&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
			D3D12_HEAP_FLAG_NONE,
			&CD3DX12_RESOURCE_DESC::Tex2D(DXGI_FORMAT_D32_FLOAT, width, height, 1, 0, 1, 0, D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL),
			D3D12_RESOURCE_STATE_DEPTH_WRITE,
			&depthOptimizedClearValue,
			IID_PPV_ARGS(&depthStencilBuffer)
		);
		_device->Device->CreateDescriptorHeap(&dsvHeapDesc, IID_PPV_ARGS(&dsDescriptorHeap));
		_device->Device->CreateDepthStencilView(depthStencilBuffer, &depthStencilDesc, dsDescriptorHeap->GetCPUDescriptorHandleForHeapStart());

		int textureMemoryUsage = RenderingHelpers::GetTextureMemoryUsage(depthStencilDesc.Format, width, height, 1);
		_memoryUsage += textureMemoryUsage;
		_device->PrivateDeviceMemoryUsage += textureMemoryUsage;

		result = RenderingContext::Resize(width, height, fullscreen);
	}
	__finally{
		Device->RemoveLocker();
	}
	return result;
}

bool RenderingContextDX12::IsFullscreen() const {
	return false;
}

void RenderingContextDX12::release() {
	WaitForPreviousFrame();
	CloseHandle(fenceEvent);

	for (int i = 0; i < FrameCount; ++i)
	{
		frameIndex = i;
		WaitForPreviousFrame();
	}

	BOOL fs = false;
	if (swapChain != NULL && swapChain->GetFullscreenState(&fs, NULL))
		swapChain->SetFullscreenState(false, NULL);


	SAFE_RELEASE(swapChain);
	SAFE_RELEASE(rtvDescriptorHeap);
	SAFE_RELEASE(commandList);

	for (int i = 0; i < FrameCount; ++i)
	{
		SAFE_RELEASE(renderTargets[i]);
		SAFE_RELEASE(commandAllocator[i]);
		SAFE_RELEASE(fence[i]);
	};

	for (std::map<HWND, SwapChainEntry>::iterator it = _swapChains.begin(); it != _swapChains.end(); ++it)
	{
		CloseHandle(it->second.fenceEvent);
		if (it->second.swapChain->GetFullscreenState(&fs, NULL))
			it->second.swapChain->SetFullscreenState(false, NULL);
		SAFE_RELEASE(it->second.swapChain);
		SAFE_RELEASE(it->second.rtvDescriptorHeap);
		SAFE_RELEASE(it->second.commandList);

		for (int i = 0; i < FrameCount; ++i)
		{
			SAFE_RELEASE(it->second.renderTargets[i]);
			SAFE_RELEASE(it->second.commandAllocator[i]);
			SAFE_RELEASE(it->second.fence[i]);
		};
	}

	_swapChains.clear();
	_device->PrivateDeviceMemoryUsage -= _memoryUsage;
	_memoryUsage = 0;
}

void RenderingContextDX12::WaitForPreviousFrame()
{
	if (swapChain != NULL) {
		frameIndex = swapChain->GetCurrentBackBufferIndex();

		if (fence[frameIndex]->GetCompletedValue() < fenceValue[frameIndex])
		{
			fence[frameIndex]->SetEventOnCompletion(fenceValue[frameIndex], fenceEvent);
			WaitForSingleObject(fenceEvent, INFINITE);
		}
		fenceValue[frameIndex]++;
	}
}

void RenderingContextDX12::SetOutput(HWND handle) {
	if (handle != output) {
		output = handle;
		if (output != 0) {
			if (_swapChains.find(output) == _swapChains.end()) {
				DXGI_MODE_DESC backBufferDesc = {};
				backBufferDesc.Width = Width();
				backBufferDesc.Height = Height();
				backBufferDesc.Format = RenderingHelpers::GetDXGIFormat(Device->DefaultBackBufferFormat());
				backBufferDesc.RefreshRate.Numerator = 60;
				backBufferDesc.RefreshRate.Denominator = 1;
				backBufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
				backBufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

				DXGI_SWAP_CHAIN_DESC swapChainDesc = {};
				swapChainDesc.BufferCount = FrameCount;
				swapChainDesc.BufferDesc = backBufferDesc;
				swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
				swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
				swapChainDesc.OutputWindow = output;
				swapChainDesc.SampleDesc.Count = 1;
				swapChainDesc.SampleDesc.Quality = 0;
				swapChainDesc.Windowed = TRUE;
				swapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

				IDXGISwapChain* tempSwapChain;
				GraphicsDevice::FactoryDXGI->CreateSwapChain(_device->Queue, &swapChainDesc, &tempSwapChain);

				int textureMemoryUsage = RenderingHelpers::GetTextureMemoryUsage(backBufferDesc.Format, backBufferDesc.Width, backBufferDesc.Height, 1) * swapChainDesc.BufferCount; //In DX12 we use more than one buffer
				_memoryUsage += textureMemoryUsage;
				_device->PrivateDeviceMemoryUsage += textureMemoryUsage;

				swapChain = static_cast<IDXGISwapChain3*>(tempSwapChain);

				frameIndex = swapChain->GetCurrentBackBufferIndex();

				D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc = {};
				rtvHeapDesc.NumDescriptors = FrameCount;
				rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
				rtvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;

				_device->Device->CreateDescriptorHeap(&rtvHeapDesc, IID_PPV_ARGS(&rtvDescriptorHeap));

				rtvDescriptorSize = _device->Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);

				CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle(rtvDescriptorHeap->GetCPUDescriptorHandleForHeapStart());
				for (int i = 0; i < FrameCount; i++)
				{
					swapChain->GetBuffer(i, IID_PPV_ARGS(&renderTargets[i]));

					_device->Device->CreateRenderTargetView(renderTargets[i], nullptr, rtvHandle);
					D3D11_RESOURCE_FLAGS d3d11Flags = { D3D11_BIND_RENDER_TARGET };
					_device->DeviceDX11OnDX12->CreateWrappedResource(
						renderTargets[i],
						&d3d11Flags,
						D3D12_RESOURCE_STATE_RENDER_TARGET,
						D3D12_RESOURCE_STATE_PRESENT,
						IID_PPV_ARGS(&wrappedBackBuffers[i])
					);

					// Create a render target for D2D to draw directly to this back buffer.
					IDXGISurface* surface;
					wrappedBackBuffers[i]->QueryInterface<IDXGISurface>(&surface);
					
					D2D1_RENDER_TARGET_PROPERTIES props = D2D1::RenderTargetProperties(D2D1_RENDER_TARGET_TYPE_DEFAULT, D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED));
					_device->FactoryDirect2D1->CreateDxgiSurfaceRenderTarget(surface, &props, &renderTargets2D[i]);
					SAFE_RELEASE(surface);
					renderTargets2D[i]->SetAntialiasMode(D2D1_ANTIALIAS_MODE_PER_PRIMITIVE);
					renderTargets2D[i]->SetTextAntialiasMode(D2D1_TEXT_ANTIALIAS_MODE_CLEARTYPE);
					
					rtvHandle.Offset(1, rtvDescriptorSize);
				}

				for (int i = 0; i < FrameCount; i++)
				{
					_device->Device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&commandAllocator[i]));
				}

				_device->Device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, commandAllocator[0], NULL, IID_PPV_ARGS(&commandList));
				commandList->Close();

				for (int i = 0; i < FrameCount; i++)
				{
					_device->Device->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&fence[i]));
					fenceValue[i] = 0;
				}
				fenceEvent = CreateEvent(nullptr, FALSE, FALSE, nullptr);

				SwapChainEntry schain;

				for (int i = 0; i < FrameCount; i++)
				{
					schain.commandAllocator[i] = commandAllocator[i];
					schain.fence[i] = fence[i];
					schain.fenceValue[i] = fenceValue[i];
					schain.renderTargets[i] = renderTargets[i];
					schain.renderTargets2D[i] = renderTargets2D[i];
					schain.wrappedBackBuffers[i] = wrappedBackBuffers[i];
				}
				schain.commandList = commandList;
				schain.fenceEvent = fenceEvent;
				schain.rtvDescriptorHeap = rtvDescriptorHeap;
				schain.rtvDescriptorSize = rtvDescriptorSize;
				schain.frameIndex = frameIndex;
				schain.swapChain = swapChain;
				_swapChains[output] = schain;
			}
			else {
				SwapChainEntry schain = _swapChains[output];
				for (int i = 0; i < FrameCount; i++)
				{
					commandAllocator[i] = schain.commandAllocator[i];
					fence[i] = schain.fence[i];
					fenceValue[i] = schain.fenceValue[i];
					renderTargets[i] = schain.renderTargets[i];
					renderTargets2D[i] = schain.renderTargets2D[i];
					wrappedBackBuffers[i] = schain.wrappedBackBuffers[i];
				}
				commandList = schain.commandList;
				fenceEvent = schain.fenceEvent;
				rtvDescriptorHeap = schain.rtvDescriptorHeap;
				rtvDescriptorSize = schain.rtvDescriptorSize;
				frameIndex = schain.frameIndex;
				swapChain = schain.swapChain;
			}

		}
	}
}

void RenderingContextDX12::Draw2D() {
	RenderTarget2D->BeginDraw();
	__try
	{
		drawGUI();
	}
	__finally
	{
		RenderTarget2D->EndDraw();
	}
}