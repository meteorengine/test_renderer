#pragma once

//Include and link appropriate libraries and headers

class RendererDX12
{
private:

	ID3D12Device* device;										// direct3d device
	IDXGISwapChain3* swapChain;									// swapchain used to switch between render targets
	ID3D12CommandQueue* commandQueue;							// container for command lists
	ID3D12DescriptorHeap* rtvDescriptorHeap;					// a descriptor heap to hold resources like the render targets
	ID3D12Resource* renderTargets[frameBufferCount];			// number of render targets equal to buffer count
	ID3D12CommandAllocator* commandAllocator[frameBufferCount]; // we want enough allocators for each buffer * number of threads (we only have one thread)
	ID3D12GraphicsCommandList* commandList;						// a command list we can record commands into, then execute them to render the frame
	ID3D12Fence* fence[frameBufferCount];						// an object that is locked while our command list is being executed by the gpu. We need as many 
																//as we have allocators (more if we want to know when the gpu is finished with an asset)
	HANDLE fenceEvent;											// a handle to an event when our fence is unlocked by the gpu
	UINT64 fenceValue[frameBufferCount];						// this value is incremented each frame. each fence will have its own value
	int frameIndex;												// current rtv we are on
	int rtvDescriptorSize;										// size of the rtv descriptor on the device (all front and back buffers will be the same size)

	void UpdatePipeline();
	void WaitForPreviousFrame();

	float red = 0.0f;
	float green = 0.0f;
	float blue = 0.0f;
	int colormodr = 1;
	int colormodg = 1;
	int colormodb = 1;

public:
	bool InitD3D(HINSTANCE hInstance, HWND hWnd, int width, int height);
	bool InitScene();
	void UpdateScene();
	void DrawScene();
	void ReleaseObjects();
};

