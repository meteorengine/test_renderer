#pragma once

class GraphicsDeviceDX12 :
	public GraphicsDevice
{
private:
	RenderingContextDX12* _currentContext;
	bool _vsync;
	ID3D11RasterizerState* _rasterizerStates[6];
	ID3D11BlendState* _blendStates[5];
	ID3D11DepthStencilState* _depthStencilStates[3];
	ID3D11Buffer* _quad_Buff;
	VertexBufferBinding* _quad_Bind;
	CULL_MODE _cullMode;
	bool _wireframe;
	BLEND_STATE _blendState;
	DEPTH_STENCIL_STATE _depthStencilState;
	int _bindedRTsCount;
	ID3D12Resource* _bindedRTs[4];
	void setContext(RenderingContext* context);
	void setupFormats();
	bool checkFormatSupport(DXGI_FORMAT format, UINT flags, bool isNecessary);
	ID3D11Device* DeviceDX11;
	ID3D11DeviceContext* ContextDX11;
public:
	ID3D12Device* Device;
	ID3D11On12Device* DeviceDX11OnDX12;
	ID3D12CommandQueue* Queue;
	GraphicsDeviceDX12(HWND hWnd);
	~GraphicsDeviceDX12();

	void Init();
	
	bool Wireframe() const;
	BLEND_STATE BlendState() const;
	DEPTH_STENCIL_STATE DepthStencilState() const;
	CULL_MODE CullMode() const;

	void SetWireframe(bool wf);
	void SetBlendState(BLEND_STATE bs);
	void SetDepthStencilState(DEPTH_STENCIL_STATE dss);
	void SetCullMode(CULL_MODE cm);
	void Draw();
	RendererType Renderer() const;
};

