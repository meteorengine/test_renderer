#pragma once
class RenderingContextDX12 :
	public RenderingContext
{
private:
	static const int FrameCount = 2;

	struct SwapChainEntry
	{
		IDXGISwapChain3* swapChain;
		ID3D12DescriptorHeap* rtvDescriptorHeap;
		ID3D12Resource* renderTargets[FrameCount];
		ID3D12CommandAllocator* commandAllocator[FrameCount];
		ID3D11Resource* wrappedBackBuffers[FrameCount];
		ID2D1RenderTarget* renderTargets2D[FrameCount];
		ID3D12GraphicsCommandList* commandList;
		ID3D12Fence* fence[FrameCount];
		UINT64 fenceValue[FrameCount];
		HANDLE fenceEvent;
		int rtvDescriptorSize;
		int frameIndex;
	};

	std::map<HWND, SwapChainEntry> _swapChains;

	GraphicsDeviceDX12* _device;

	int _memoryUsage;
	void release();
public:
	D3D12_VIEWPORT viewport;

	ID3D12Resource* depthStencilBuffer;
	ID3D12DescriptorHeap* dsDescriptorHeap;
	ID3D11Resource* wrappedBackBuffers[FrameCount];
	ID2D1RenderTarget* renderTargets2D[FrameCount];

	IDXGISwapChain3* swapChain;
	ID3D12DescriptorHeap* rtvDescriptorHeap;
	ID3D12Resource* renderTargets[FrameCount];
	ID3D12CommandAllocator* commandAllocator[FrameCount];
	ID3D12GraphicsCommandList* commandList;
	ID3D12Fence* fence[FrameCount];
	UINT64 fenceValue[FrameCount];
	HANDLE fenceEvent;
	int rtvDescriptorSize;
	int frameIndex;

	RenderingContextDX12(GraphicsDevice* device, HWND output, std::string name, bool main);
	bool Resize(int width, int height, bool fullscreen);
	bool IsFullscreen() const;
	void SetOutput(HWND handle);
	void Draw2D();
	void WaitForPreviousFrame();
	~RenderingContextDX12();
};

