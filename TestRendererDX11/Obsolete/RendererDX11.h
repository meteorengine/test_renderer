#pragma once

class RendererDX11
{
private:
	IDXGISwapChain* SwapChain;
	ID3D11Device* d3d11Device;
	ID3D11DeviceContext* d3d11DevCon;
	ID3D11RenderTargetView* renderTargetView;

	float red = 0.0f;
	float green = 0.0f;
	float blue = 0.0f;
	int colormodr = 1;
	int colormodg = 1;
	int colormodb = 1;
public:
	bool InitD3D(HINSTANCE hInstance, HWND hWnd, int width, int height);
	bool InitScene();
	void UpdateScene();
	void DrawScene();
	void ReleaseObjects();
};
