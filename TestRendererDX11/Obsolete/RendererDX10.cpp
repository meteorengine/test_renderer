#include "Include_General.h"
#include "Include_Rendering.h"

#include "RendererDX10.h"

bool RendererDX10::InitD3D(HINSTANCE hInstance, HWND hWnd, int width, int height) {

	SetWindowText(hWnd, "Testing renderer [DX10]");

	DXGI_SWAP_CHAIN_DESC scd;
	scd.BufferDesc.Width = width;
	scd.BufferDesc.Height = height;
	scd.BufferDesc.RefreshRate.Numerator = 60;
	scd.BufferDesc.RefreshRate.Denominator = 1;
	scd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	scd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	scd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

	//no multisampling
	scd.SampleDesc.Count = 1;
	scd.SampleDesc.Quality = 0;

	scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	scd.BufferCount = 2;
	scd.OutputWindow = hWnd;
	scd.Windowed = true;
	scd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	scd.Flags = 0;

	IDXGIAdapter* adapter = Adapter::FindBest()->AdapterDXGI;

	D3D10CreateDeviceAndSwapChain(adapter, D3D10_DRIVER_TYPE_HARDWARE, 0, 0, D3D10_SDK_VERSION, &scd, &SwapChain, &d3dDevice);

	ID3D10Texture2D* backBuffer;
	SwapChain->GetBuffer(0, _uuidof(ID3D10Texture2D), reinterpret_cast<void**>(&backBuffer));
	d3dDevice->CreateRenderTargetView(backBuffer, 0, &RenderTargetView);
	backBuffer->Release();

	d3dDevice->OMSetRenderTargets(1, &RenderTargetView, NULL);

	D3D10_VIEWPORT vp;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	vp.Width = width;
	vp.Height = height;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;

	d3dDevice->RSSetViewports(1, &vp);

	D3DXCOLOR bgColor(0.0f, 0.0f, 0.0f, 1.0f);
	d3dDevice->ClearRenderTargetView(RenderTargetView, bgColor);

	SwapChain->Present(0, 0);

	return true;
}


bool RendererDX10::InitScene() {
	return true;
}

void RendererDX10::UpdateScene() {
	red += colormodr * 0.00005f;
	green += colormodg * 0.00002f;
	blue += colormodb * 0.00001f;

	if (red >= 1.0f || red <= 0.0f)
		colormodr *= -1;
	if (green >= 1.0f || green <= 0.0f)
		colormodg *= -1;
	if (blue >= 1.0f || blue <= 0.0f)
		colormodb *= -1;
}

void RendererDX10::DrawScene() {
	D3DXCOLOR bgColor(red, green, blue, 1.0f);

	d3dDevice->ClearRenderTargetView(RenderTargetView, bgColor);

	SwapChain->Present(0, 0);
}

void RendererDX10::ReleaseObjects() {
	SwapChain->Release();
	d3dDevice->Release();
}