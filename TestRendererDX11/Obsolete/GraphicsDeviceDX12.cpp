#include "stdafx.h"
#include "AdapterInfo.h"
#include "RendererType.h"
#include "DeviceState.h"
#include "PixelFormat.h"
#include "PixelFormatInfo.h"
#include "VertexBufferBinding.h"
#include "CullMode.h"
#include "BlendState.h"
#include "DepthStencilState.h"
#include "Adapter.h"
class GraphicsDevice;
#include "RenderingContext.h"
#include "GraphicsDevice.h"

class GraphicsDeviceDX12;
#include "RenderingContextDX12.h"
#include "GraphicsDeviceDX12.h"
GraphicsDeviceDX12::GraphicsDeviceDX12(HWND hWnd) : GraphicsDevice(hWnd)
{
	D3D12CreateDevice(Adapter::FindBest()->AdapterDXGI, D3D_FEATURE_LEVEL_12_0, IID_PPV_ARGS(&Device));

	D3D12_COMMAND_QUEUE_DESC cqDesc = {};
	cqDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
	cqDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;

	Device->CreateCommandQueue(&cqDesc, IID_PPV_ARGS(&Queue));
	D3D11On12CreateDevice(Device, D3D11_CREATE_DEVICE_BGRA_SUPPORT, NULL, 0, reinterpret_cast<IUnknown**>(&Queue), 1, 0, &DeviceDX11, &ContextDX11, NULL);
	DeviceDX11->QueryInterface(IID_PPV_ARGS(&DeviceDX11OnDX12));

	setupFormats();

	_currentContext = new RenderingContextDX12(this, hWnd, "Main Context", true);
	mainContext = _currentContext;

	if (_currentContext->Resize(_initialWidth, _initialHeight, false)) {
		_currentContext = NULL;
		MessageBox(0, "Cannot create Main rendering Context", "Error", MB_OK);
	}
}

GraphicsDeviceDX12::~GraphicsDeviceDX12()
{
	SAFE_RELEASE(Queue);
	SAFE_RELEASE(DeviceDX11);
	SAFE_RELEASE(Device);
}

void GraphicsDeviceDX12::Init() {
	if (_state != DeviceState::Created) {
		return;
	}
	_bindedRTsCount = 0;
	setContext(mainContext);
	_state = DeviceState::Inited;
}

void GraphicsDeviceDX12::setupFormats() {
	//Check if required formats are availible
	checkFormatSupport(DXGI_FORMAT_R8G8B8A8_UNORM, D3D11_FORMAT_SUPPORT_MIP | D3D11_FORMAT_SUPPORT_RENDER_TARGET | D3D11_FORMAT_SUPPORT_DISPLAY, true);
	checkFormatSupport(DXGI_FORMAT_R16G16B16A16_FLOAT, D3D11_FORMAT_SUPPORT_MIP | D3D11_FORMAT_SUPPORT_RENDER_TARGET | D3D11_FORMAT_SUPPORT_BLENDABLE, true);
	checkFormatSupport(DXGI_FORMAT_R16_FLOAT, D3D11_FORMAT_SUPPORT_MIP | D3D11_FORMAT_SUPPORT_RENDER_TARGET | D3D11_FORMAT_SUPPORT_SHADER_GATHER, true);
	checkFormatSupport(DXGI_FORMAT_R16_UNORM, D3D11_FORMAT_SUPPORT_SHADER_SAMPLE_COMPARISON | D3D11_FORMAT_SUPPORT_MIP | D3D11_FORMAT_SUPPORT_RENDER_TARGET | D3D11_FORMAT_SUPPORT_SHADER_GATHER | D3D11_FORMAT_SUPPORT_SHADER_GATHER_COMPARISON, true);
	checkFormatSupport(DXGI_FORMAT_R32_FLOAT, D3D11_FORMAT_SUPPORT_MIP | D3D11_FORMAT_SUPPORT_RENDER_TARGET | D3D11_FORMAT_SUPPORT_SHADER_GATHER, true);
	checkFormatSupport(DXGI_FORMAT_R10G10B10A2_TYPELESS, D3D11_FORMAT_SUPPORT_MIP, true);
	checkFormatSupport(DXGI_FORMAT_R11G11B10_FLOAT, D3D11_FORMAT_SUPPORT_MIP | D3D11_FORMAT_SUPPORT_RENDER_TARGET, true);
	checkFormatSupport(DXGI_FORMAT_BC1_TYPELESS, D3D11_FORMAT_SUPPORT_MIP, true);
	checkFormatSupport(DXGI_FORMAT_BC2_TYPELESS, D3D11_FORMAT_SUPPORT_MIP, true);
	checkFormatSupport(DXGI_FORMAT_BC3_TYPELESS, D3D11_FORMAT_SUPPORT_MIP, true);
	checkFormatSupport(DXGI_FORMAT_BC4_TYPELESS, D3D11_FORMAT_SUPPORT_MIP, true);
	checkFormatSupport(DXGI_FORMAT_BC5_TYPELESS, D3D11_FORMAT_SUPPORT_MIP, true);
	checkFormatSupport(DXGI_FORMAT_D32_FLOAT, D3D11_FORMAT_SUPPORT_MIP | D3D11_FORMAT_SUPPORT_DEPTH_STENCIL, true);

	//Check if optional formats are availible
	Formats[1]->PlatformFormat = DXGI_FORMAT_R8G8B8A8_UNORM;
	Formats[2]->PlatformFormat = DXGI_FORMAT_R16G16B16A16_FLOAT;
	Formats[3]->PlatformFormat = DXGI_FORMAT_R32G32B32A32_FLOAT;
	Formats[3]->IsSupported = checkFormatSupport(DXGI_FORMAT_R32G32B32A32_FLOAT, D3D11_FORMAT_SUPPORT_MIP | D3D11_FORMAT_SUPPORT_RENDER_TARGET, false);
	Formats[4]->PlatformFormat = DXGI_FORMAT_A8_UNORM;
	Formats[4]->IsSupported = checkFormatSupport(DXGI_FORMAT_A8_UNORM, D3D11_FORMAT_SUPPORT_MIP | D3D11_FORMAT_SUPPORT_RENDER_TARGET | D3D11_FORMAT_SUPPORT_SHADER_GATHER, false);
	Formats[5]->PlatformFormat = DXGI_FORMAT_R16_FLOAT;
	Formats[6]->PlatformFormat = DXGI_FORMAT_R32_FLOAT;
	Formats[6]->IsSupported = checkFormatSupport(DXGI_FORMAT_R32_FLOAT, D3D11_FORMAT_SUPPORT_MIP | D3D11_FORMAT_SUPPORT_RENDER_TARGET | D3D11_FORMAT_SUPPORT_SHADER_GATHER, false);
	Formats[7]->PlatformFormat = DXGI_FORMAT_R16G16_FLOAT;
	Formats[7]->IsSupported = checkFormatSupport(DXGI_FORMAT_R16G16_FLOAT, D3D11_FORMAT_SUPPORT_MIP | D3D11_FORMAT_SUPPORT_RENDER_TARGET, false);
	Formats[8]->PlatformFormat = DXGI_FORMAT_R32G32_FLOAT;
	Formats[8]->IsSupported = checkFormatSupport(DXGI_FORMAT_R32G32_FLOAT, D3D11_FORMAT_SUPPORT_MIP | D3D11_FORMAT_SUPPORT_RENDER_TARGET, false);
	Formats[9]->PlatformFormat = DXGI_FORMAT_R10G10B10A2_UNORM;
	Formats[10]->PlatformFormat = DXGI_FORMAT_R11G11B10_FLOAT;
	Formats[11]->PlatformFormat = DXGI_FORMAT_BC1_UNORM;
	Formats[12]->PlatformFormat = DXGI_FORMAT_BC2_UNORM;
	Formats[13]->PlatformFormat = DXGI_FORMAT_BC3_UNORM;
	Formats[14]->PlatformFormat = DXGI_FORMAT_BC4_UNORM;
	Formats[15]->PlatformFormat = DXGI_FORMAT_BC5_UNORM;
	Formats[16]->PlatformFormat = DXGI_FORMAT_B8G8R8A8_UNORM;
	Formats[16]->IsSupported = checkFormatSupport(DXGI_FORMAT_B8G8R8A8_UNORM, D3D11_FORMAT_SUPPORT_MIP | D3D11_FORMAT_SUPPORT_RENDER_TARGET | D3D11_FORMAT_SUPPORT_DISPLAY, false);
	Formats[17]->PlatformFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
	Formats[18]->PlatformFormat = DXGI_FORMAT_R32_FLOAT;
	Formats[19]->PlatformFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
	Formats[19]->IsSupported = checkFormatSupport(DXGI_FORMAT_D24_UNORM_S8_UINT, D3D11_FORMAT_SUPPORT_MIP | D3D11_FORMAT_SUPPORT_DEPTH_STENCIL, false);
	Formats[20]->PlatformFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
	Formats[21]->PlatformFormat = DXGI_FORMAT_D32_FLOAT;
	Formats[22]->PlatformFormat = DXGI_FORMAT_R16_UNORM;
	Formats[23]->PlatformFormat = DXGI_FORMAT_B5G6R5_UNORM;
	Formats[23]->IsSupported = checkFormatSupport(DXGI_FORMAT_B5G6R5_UNORM, D3D11_FORMAT_SUPPORT_MIP, false);
}

bool GraphicsDeviceDX12::checkFormatSupport(DXGI_FORMAT format, UINT flags, bool isNecessary) {
	//TODO: Check for flags (if possible)

	D3D12_FEATURE_DATA_FORMAT_INFO formatInfo = { format };
	HRESULT supported = Device->CheckFeatureSupport(D3D12_FEATURE_FORMAT_INFO, &formatInfo, sizeof(formatInfo));
	flags |= D3D11_FORMAT_SUPPORT_TEXTURE2D;
	if (isNecessary && FAILED(supported)) {
		MessageBox(0, "Format XXX is not supported on this platform.", "Error", MB_OK);
		//TODO: Exit game
	}
	return SUCCEEDED(supported);
}

RendererType GraphicsDeviceDX12::Renderer() const {
	return RendererType::DirectX_12;
}

bool GraphicsDeviceDX12::Wireframe() const {
	return _wireframe;
}

BLEND_STATE GraphicsDeviceDX12::BlendState() const {
	return _blendState;
}

DEPTH_STENCIL_STATE GraphicsDeviceDX12::DepthStencilState() const {
	return _depthStencilState;
}

CULL_MODE GraphicsDeviceDX12::CullMode() const {
	return _cullMode;
}

void GraphicsDeviceDX12::SetWireframe(bool wf) {
}

void GraphicsDeviceDX12::SetBlendState(BLEND_STATE bs) {
}
void GraphicsDeviceDX12::SetDepthStencilState(DEPTH_STENCIL_STATE dss) {
}

void GraphicsDeviceDX12::SetCullMode(CULL_MODE cm) {
}

//HUGE TODO: FIX _com_error, details availible whe uncommenting Mesasgebox in Draw()

void GraphicsDeviceDX12::setContext(RenderingContext* context) {
	_currentContext = (RenderingContextDX12*)context;
	//_currentContext->WaitForPreviousFrame(); //TODO: Fix
	_currentContext->commandAllocator[_currentContext->frameIndex]->Reset();
	_currentContext->commandList->Reset(_currentContext->commandAllocator[_currentContext->frameIndex], NULL);
	_currentContext->commandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(_currentContext->renderTargets[_currentContext->frameIndex], D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET));
	CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle(_currentContext->rtvDescriptorHeap->GetCPUDescriptorHandleForHeapStart(), _currentContext->frameIndex, _currentContext->rtvDescriptorSize);
	CD3DX12_CPU_DESCRIPTOR_HANDLE dsvHandle(_currentContext->dsDescriptorHeap->GetCPUDescriptorHandleForHeapStart());
	_currentContext->commandList->OMSetRenderTargets(1, &rtvHandle, FALSE, &dsvHandle);
	const float clearColor[] = { 1, 0, 0, 1.0f };
	_currentContext->commandList->ClearRenderTargetView(rtvHandle, clearColor, 0, nullptr);
	_currentContext->commandList->ClearDepthStencilView(_currentContext->dsDescriptorHeap->GetCPUDescriptorHandleForHeapStart(), D3D12_CLEAR_FLAG_DEPTH, 1.0f, 0, 0, nullptr);
	_currentContext->commandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(_currentContext->renderTargets[_currentContext->frameIndex], D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT));
	_currentContext->commandList->Close();
}

//THIS IS TEMP FUNCTION, JUST TO TEST
void GraphicsDeviceDX12::Draw() {
	setContext(mainContext);
	HRESULT hr;
	ID3D12CommandList* ppCommandLists[] = { _currentContext->commandList };
	Queue->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);
	Queue->Signal(_currentContext->fence[_currentContext->frameIndex], _currentContext->fenceValue[_currentContext->frameIndex]);

	DeviceDX11OnDX12->AcquireWrappedResources(&_currentContext->wrappedBackBuffers[_currentContext->frameIndex], 1);
	_currentContext->renderTargets2D[_currentContext->frameIndex]->BeginDraw();
	_currentContext->renderTargets2D[_currentContext->frameIndex]->EndDraw();
	DeviceDX11OnDX12->ReleaseWrappedResources(&_currentContext->wrappedBackBuffers[_currentContext->frameIndex], 1);

	hr = _currentContext->swapChain->Present(0, 0);
	/*if (FAILED(hr)) {
		hr = Device->GetDeviceRemovedReason();
		_com_error error(hr);
		MessageBox(0, error.ErrorMessage(),
			(std::string("Error: ") + std::to_string(__LINE__)).c_str(),
			MB_OK);
	}*/
}