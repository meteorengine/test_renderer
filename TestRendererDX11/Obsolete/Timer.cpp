#include "Timer.h"

Timer::Timer()
{
	LARGE_INTEGER li;
	QueryPerformanceFrequency(&li);

	// seconds
	//timerFrequency = double(li.QuadPart);

	// milliseconds
	timerFrequency = double(li.QuadPart) / 1000.0;

	// microseconds
	//timerFrequency = double(li.QuadPart) / 1000000.0;

	QueryPerformanceCounter(&li);
	lastFrameTime = li.QuadPart;
}

double Timer::GetFrameDelta() {
	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	frameDelta = double(li.QuadPart - lastFrameTime) / timerFrequency;
	if (frameDelta > 0)
		fps = 1000.0 / frameDelta;
	lastFrameTime = li.QuadPart;
	return frameDelta;
}