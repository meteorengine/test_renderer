﻿#include "Include_General.h"
#include "Include_Rendering.h"
#include "Include_Rendering_DX11.h"

//Define variables/constants
LPCTSTR WndClassName	= "firstwindow";
HWND hwnd				= NULL;

const int Width			= 1280;										//window width
const int Height		= 720;										//window height
GraphicsDeviceDX11* graphicsDevice = NULL;


bool InitializeWindow(HINSTANCE hInstance,							//Initialize our window
	int ShowWnd,
	int width,
	int height,
	bool windowed);

int messageloop();

LRESULT CALLBACK WndProc(HWND hWnd,									//Windows callback procedure
	UINT msg,
	WPARAM wParam,
	LPARAM lParam);

int WINAPI WinMain(HINSTANCE hInstance,								//Main windows function
	HINSTANCE hPrevInstance,
	LPSTR lpCmdLine,
	int nShowCmd)
{
	if (!InitializeWindow(hInstance, nShowCmd, Width, Height, true))
	{
		MessageBox(0, "Window Initialization - Failed",
			"Error", MB_OK);
		return 0;
	}
	graphicsDevice = new GraphicsDeviceDX11(hwnd);
	graphicsDevice->Init();

	messageloop();													//Jump into the heart of our program

	return 0;
}
void SetClientSize(HWND hwnd, int clientWidth, int clientHeight)
{
	if (IsWindow(hwnd))
	{

		DWORD dwStyle = (DWORD)GetWindowLongPtr(hwnd, GWL_STYLE);
		DWORD dwExStyle = (DWORD)GetWindowLongPtr(hwnd, GWL_EXSTYLE);
		HMENU menu = GetMenu(hwnd);

		RECT rc = { 0, 0, clientWidth, clientHeight };

		if (!AdjustWindowRectEx(&rc, dwStyle, menu ? TRUE : FALSE, dwExStyle))
			MessageBox(NULL, "AdjustWindowRectEx Failed!", "Error", MB_OK);

		SetWindowPos(hwnd, NULL, 0, 0, rc.right - rc.left, rc.bottom - rc.top,
			SWP_NOZORDER | SWP_NOMOVE);
		#ifdef _DEBUG    
		RECT newClientRC;
		GetClientRect(hwnd, &newClientRC);
		assert((newClientRC.right - newClientRC.left) == clientWidth);
		assert((newClientRC.bottom - newClientRC.top) == clientHeight);
		#endif
	}
}
bool InitializeWindow(HINSTANCE hInstance,							//Initialize our window
	int ShowWnd,
	int width,
	int height,
	bool windowed)
{
																	//Start creating the window

	WNDCLASSEX wc;													//Create a new extended windows class

	wc.cbSize			= sizeof(WNDCLASSEX);						//Size of our windows class
	wc.style			= CS_HREDRAW | CS_VREDRAW;					//class styles
	wc.lpfnWndProc		= WndProc;									//Default windows procedure function
	wc.cbClsExtra		= NULL;										//Extra bytes after our wc structure
	wc.cbWndExtra		= NULL;										//Extra bytes after our windows instance
	wc.hInstance		= hInstance;								//Instance to current application
	wc.hIcon			= LoadIcon(NULL, IDI_WINLOGO);				//Title bar Icon
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);				//Default mouse Icon
	wc.hbrBackground	= (HBRUSH)(COLOR_WINDOW + 2);				//Window bg color
	wc.lpszMenuName		= NULL;										//Name of the menu attached to our window
	wc.lpszClassName	= WndClassName;								//Name of our windows class
	wc.hIconSm			= LoadIcon(NULL, IDI_WINLOGO);				//Icon in your taskbar

	if (!RegisterClassEx(&wc))										//Register our windows class
	{
		MessageBox(NULL, "Error registering class",
			"Error", MB_OK | MB_ICONERROR);
		return 1;
	}
	hwnd = CreateWindowEx(											//Create our Extended Window
		NULL,														//Extended style
		WndClassName,												//Name of our windows class
		"",															//Name in the title bar of our window
		WS_OVERLAPPEDWINDOW,										//style of our window
		CW_USEDEFAULT, CW_USEDEFAULT,								//Top left corner of window
		width,														//Width of our window
		height,														//Height of our window
		NULL,														//Handle to parent window
		NULL,														//Handle to a Menu
		hInstance,													//Specifies instance of current program
		NULL														//used for an MDI client window
	);

	if (!hwnd)														//Make sure our window has been created
	{
		MessageBox(NULL, "Error creating window",
			"Error", MB_OK | MB_ICONERROR);
		return 1;
	}

	ShowWindow(hwnd, ShowWnd);
	UpdateWindow(hwnd);	
	SetClientSize(hwnd, 1280, 720);
	return true;
}

int messageloop() {													//The message loop

	MSG msg;														//Create a new message structure

	while (true)													//while there is a message
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)								//if the message was WM_QUIT
				break;												//Exit the message loop

			TranslateMessage(&msg);									//Translate the message
			DispatchMessage(&msg);									//Send the message to default windows procedure
		}
		else {														//Run "Game" code
			graphicsDevice->Draw();
		}
	}
	return (int)msg.wParam;											//return the message
}

LRESULT CALLBACK WndProc(HWND hwnd,									//Default windows procedure
	UINT msg,
	WPARAM wParam,
	LPARAM lParam)
{
	switch (msg)													//Check message
	{
		case WM_DESTROY:											//Window X button pressed
		{
			PostQuitMessage(0);
			return 0;
		}
	}
	return DefWindowProc(hwnd,
		msg,
		wParam,
		lParam);
}