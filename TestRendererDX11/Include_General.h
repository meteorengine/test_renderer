#pragma once
#ifdef _DEBUG 
// -- Include DX Libraries -- //
#pragma comment(lib, "d3d10.lib")
#pragma comment(lib, "d3dx10d.lib")
#pragma comment(lib, "d3d10_1.lib")
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dx11d.lib")
#pragma comment(lib, "Effects11d.lib")
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d2d1.lib")
#pragma comment(lib, "dwrite.lib")
// -- Include other libraries -- //
#pragma comment(lib, "comsuppwd.lib")
#pragma comment(lib, "zlib.lib")
#else
// -- Include DX Libraries -- //
#pragma comment(lib, "d3d10.lib")
#pragma comment(lib, "d3dx10.lib")
#pragma comment(lib, "d3d10_1.lib")
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dx11.lib")
#pragma comment(lib, "Effects11.lib")
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d2d1.lib")
#pragma comment(lib, "dwrite.lib")
// -- Include other libraries -- //
#pragma comment(lib, "comsuppw.lib")
#pragma comment(lib, "zlib.lib")
#endif // _DEBUG 

// -- Include DX Headers -- //

// DX10
#include <D3D10_1.h>
#include <D3D10.h>
#include <d3dx10.h>

//DX11
#include <d3d11.h>
#include <d3dx11.h>
#include <D3dx11effect.h>

//DXGI 1.4
#include <dxgi1_4.h>

//D3D Compiler
#include <D3Dcommon.h>
#include <D3Dcompiler.h>

//Direct2D
#include <d2d1.h>

//DirectWrite
#include <dwrite.h>

//Math
#include <DirectXMath.h>

// -- Include other headers -- //
#include <Windows.h>
#include <string>
#include <vector>
#include <cassert>
#include <array>
#include <map>
#include <thread>
#include <mutex>
#include <functional>
#include <comdef.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <zlib.h>
#include <ctime>
#include <zconf.h>
// -- Utility functions -- //

#define stringify(x) #x
#define SAFE_RELEASE(p) { if ( (p) ) { (p)->Release(); (p) = 0; } }

// -- Temporary imports -- //
#include "Core\DateTime.h"
#include "Core\MySerialization.h"
