#include "..\Include_General.h"
int DateTime::DaysToMonth365[] = { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365 };
int DateTime::DaysToMonth366[] = { 0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335, 366 };


DateTime::DateTime(UINT64 dateData) {
	this->dateData = dateData;
}

DateTime::DateTime(long long ticks) {
	dateData = (UINT64)ticks;
}


DateTime* DateTime::FromBinary(long long dateData) {
	//TODO: Possibly add timezone conversion
	return new DateTime((UINT64)dateData);
}

INT64 DateTime::InternalTricks() {
		return (INT64)(dateData & TicksMask);
}

int DateTime::GetDatePart(int part) {
	INT64 ticks = InternalTricks();
	// n = number of days since 1/1/0001
	int n = (int)(ticks / TicksPerDay);
	// y400 = number of whole 400-year periods since 1/1/0001
	int y400 = n / DaysPer400Years;
	// n = day number within 400-year period
	n -= y400 * DaysPer400Years;
	// y100 = number of whole 100-year periods within 400-year period
	int y100 = n / DaysPer100Years;
	// Last 100-year period has an extra day, so decrement result if 4
	if (y100 == 4) y100 = 3;
	// n = day number within 100-year period
	n -= y100 * DaysPer100Years;
	// y4 = number of whole 4-year periods within 100-year period
	int y4 = n / DaysPer4Years;
	// n = day number within 4-year period
	n -= y4 * DaysPer4Years;
	// y1 = number of whole years within 4-year period
	int y1 = n / DaysPerYear;
	// Last year has an extra day, so decrement result if 4
	if (y1 == 4) y1 = 3;
	// If year was requested, compute and return it
	if (part == DatePartYear) {
		return y400 * 400 + y100 * 100 + y4 * 4 + y1 + 1;
	}
	// n = day number within year
	n -= y1 * DaysPerYear;
	// If day-of-year was requested, return it
	if (part == DatePartDayOfYear) return n + 1;
	// Leap year calculation looks different from IsLeapYear since y1, y4,
	// and y100 are relative to year 1, not year 0
	bool leapYear = y1 == 3 && (y4 != 24 || y100 == 3);
	// All months have less than 32 days, so n >> 5 is a good conservative
	// estimate for the month
	int m = n >> 5 + 1;
	// m = 1-based month number
	while (n >= (leapYear ? DaysToMonth366 : DaysToMonth365)[m]) m++;
	// If month was requested, return it
	if (part == DatePartMonth) return m;
	// Return 1-based day-of-month
	return n - (leapYear ? DaysToMonth366 : DaysToMonth365)[m - 1] + 1;
	
}

int DateTime::Day() {
	return GetDatePart(DatePartDay);
}

int DateTime::DayOfYear() {
	return GetDatePart(DatePartDayOfYear);
}

int DateTime::Hour() {
	return (int)((InternalTricks() / TicksPerHour) % 24);
}

int DateTime::Millisecond() {
	return (int)((InternalTricks() / TicksPerMillisecond) % 1000);
}

int DateTime::Minute() {
	return (int)((InternalTricks() / TicksPerMinute) % 60);
}

int DateTime::Month() {
	return GetDatePart(DatePartMonth);
}

int DateTime::Second() {
	return (int)((InternalTricks() / TicksPerSecond) % 60);
}

int DateTime::Year() {
	return GetDatePart(DatePartYear);
}

long long DateTime::Ticks() {
	return InternalTricks();
}

INT64 DateTime::ToBinary() {
	return (INT64)dateData;
}
DateTime* DateTime::Now() {
	std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
	time_t tt = std::chrono::system_clock::to_time_t(now);	
	tm time;
	gmtime_s(&time,&tt);
	time.tm_year += 1900;
	return new DateTime(time.tm_year,time.tm_mon+1,time.tm_mday,time.tm_min,time.tm_sec,0);
}
DateTime::DateTime(int year, int month, int day, int hour, int minute, int second) {
	INT64 ticks = DateToTicks(year, month, day) + TimeToTicks(hour, minute, second);
	dateData = (UINT64)ticks;
}
DateTime::DateTime(int year, int month, int day,int hour, int minute, int second, int millisecond) {
	INT64 ticks = DateToTicks(year, month, day) + TimeToTicks(hour, minute, second);
	ticks += millisecond * TicksPerMillisecond;
	dateData = (UINT64)ticks;
}

long long DateTime::DateToTicks(int year, int month, int day)
{
	int y = year - 1;
	int n = y * 365 + y / 4 - y / 100 + y / 400 + (IsLeapYear(year) ? DaysToMonth366 : DaysToMonth365)[month - 1] + day - 1;
	return n * TicksPerDay;
}
long long DateTime::TimeToTicks(int hour, int minute, int second)
{
	long totalSeconds = (long long)hour * 3600 + (long long)minute * 60 + (long long)second;
	return totalSeconds * TicksPerSecond;
}
bool DateTime::IsLeapYear(int year) 
{
	return year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
}
