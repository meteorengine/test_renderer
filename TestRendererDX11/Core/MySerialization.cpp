#include "..\Include_General.h"

void MySerialization::WriteByte(std::ostream& stream, byte value)
{
	stream.write((char*)&value, 1);
}

void MySerialization::Write(std::ostream& stream, int value) {
	stream.write((char*)&value, 4);
}

void MySerialization::Write(std::ostream& stream, bool value) {
	stream.write((char*)&value, 1);
}

void MySerialization::Write(std::ostream& stream, DateTime* value) {
	INT64 dt = value->ToBinary();
	stream.write((char*)&dt, 8);
}

std::string MySerialization::ReadText(std::istream& stream, int length) {
	std::string text;
	while (stream.peek() != EOF && length > 0) {
		char num;
		stream.read(&num, 1);
		if (num <= 0) {
			break;
		}
		text += num;
		length--;
	}
	return text;
}

int MySerialization::ReadByte(std::istream& stream) {
	char b;
	stream.read(&b, 1);
	return (int)b;
}

bool MySerialization::ReadBool(std::istream& stream) {
	return ReadByte(stream) == 1;
}

int MySerialization::ReadInt(std::istream& stream) {
	int result;
	stream.read((char*)&result, 4);
	return result;
}

long long MySerialization::ReadLong(std::istream& stream) {
	long long result;
	stream.read((char*)&result, 8);
	return result;
}

DateTime* MySerialization::ReadDateTime(std::istream& stream) {
	return DateTime::FromBinary(ReadLong(stream));
}