#pragma once
class MySerialization
{
public:
	//-- Write --//
	static void WriteByte(std::ostream& stream, byte value);
	static void Write(std::ostream& stream, int value);
	static void Write(std::ostream& stream, bool value);
	static void Write(std::ostream& stream, DateTime* value);
	//-- Read --//
	static int ReadByte(std::istream& stream);
	static std::string ReadText(std::istream& stream, int length);
	static bool ReadBool(std::istream& stream);
	static int ReadInt(std::istream& stream);
	static long long ReadLong(std::istream& stream);
	static DateTime* ReadDateTime(std::istream& stream);
};

