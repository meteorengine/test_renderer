#pragma once
/*

	Many parts of code taken from: https://referencesource.microsoft.com/#mscorlib/system/datetime.cs
	Because of compatibility with C# version of engine

*/
class DateTime
{
private:
	int ticks;
	int GetDatePart(int part);

	// Number of 100ns ticks per time unit
	static const long long TicksPerMillisecond = 10000;
	static const long long TicksPerSecond = TicksPerMillisecond * 1000;
	static const long long TicksPerMinute = TicksPerSecond * 60;
	static const long long TicksPerHour = TicksPerMinute * 60;
	static const long long TicksPerDay = TicksPerHour * 24;

	// Number of milliseconds per time unit
	static const int MillisPerSecond = 1000;
	static const int MillisPerMinute = MillisPerSecond * 60;
	static const int MillisPerHour = MillisPerMinute * 60;
	static const int MillisPerDay = MillisPerHour * 24;

	// Number of days in a non-leap year
	static const int DaysPerYear = 365;
	// Number of days in 4 years
	static const int DaysPer4Years = DaysPerYear * 4 + 1;       // 1461
	// Number of days in 100 years
	static const int DaysPer100Years = DaysPer4Years * 25 - 1;  // 36524
	// Number of days in 400 years
	static const int DaysPer400Years = DaysPer100Years * 4 + 1; // 146097

	// Number of days from 1/1/0001 to 12/31/1600
	static const int DaysTo1601 = DaysPer400Years * 4;          // 584388
	// Number of days from 1/1/0001 to 12/30/1899
	static const int DaysTo1899 = DaysPer400Years * 4 + DaysPer100Years * 3 - 367;
	// Number of days from 1/1/0001 to 12/31/1969
	static const int DaysTo1970 = DaysPer400Years * 4 + DaysPer100Years * 3 + DaysPer4Years * 17 + DaysPerYear; // 719,162
	// Number of days from 1/1/0001 to 12/31/9999
	static const int DaysTo10000 = DaysPer400Years * 25 - 366;  // 3652059

	static const long long MinTicks = 0;
	static const long long MaxTicks = DaysTo10000 * TicksPerDay - 1;
	static const long long MaxMillis = (long long)DaysTo10000 * MillisPerDay;


	static const int DatePartYear = 0;
	static const int DatePartDayOfYear = 1;
	static const int DatePartMonth = 2;
	static const int DatePartDay = 3;

	static int DaysToMonth365[];
	static int DaysToMonth366[];

	static const UINT64 TicksMask				= 0x3FFFFFFFFFFFFFFF;
	static const UINT64 FlagsMask				= 0xC000000000000000;
	static const UINT64 LocalMask				= 0x8000000000000000;
	static const INT64 TicksCeiling				= 0x4000000000000000;
	static const UINT64 KindUnspecified			= 0x0000000000000000;
	static const UINT64 KindUtc					= 0x4000000000000000;
	static const UINT64 KindLocal				= 0x8000000000000000;
	static const UINT64 KindLocalAmbiguousDst	= 0xC000000000000000;
	static const int KindShift = 62;

	// The data is stored as an unsigned 64-bit integeter
	//   Bits 01-62: The value of 100-nanosecond ticks where 0 represents 1/1/0001 12:00am, up until the value
	//               12/31/9999 23:59:59.9999999
	//   Bits 63-64: A four-state value that describes the DateTimeKind value of the date time, with a 2nd
	//               value for the rare case where the date time is local, but is in an overlapped daylight
	//               savings time hour and it is in daylight savings time. This allows distinction of these
	//               otherwise ambiguous local times and prevents data loss when round tripping from Local to
	//               UTC time.
	UINT64 dateData;

	 DateTime(UINT64 dateData);
	 DateTime(long long ticks);

	 INT64 InternalTricks();			//Internal Ticks.... renamed by request of marcizhu on TheCherno slack

	 static long long DateToTicks(int year, int month, int day);
	 static long long TimeToTicks(int hour, int minute, int second);
public:
	static DateTime* FromBinary(long long ticks);
	int Day();
	int DayOfYear();
	int Hour();
	int Millisecond();
	int Minute();
	int Month();
	int Second();
	int Year();
	long long Ticks();
	INT64 ToBinary();
	static DateTime* Now();

	DateTime(int year, int month, int day, int hour, int minute, int second);
	DateTime(int year, int month, int day, int hour, int minute, int second, int millisecond);
	static bool IsLeapYear(int year);
};

