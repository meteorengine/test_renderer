#pragma once

//TODO: Move to DX11 (maybe)

struct VertexBufferBinding														// Vertex Buffer structure, containing binding info for D3D11 Buffer
{
	ID3D11Buffer* Buffer;														// D3D11 Buffer itself
	int Stride;																	// Stride
	int Offset;																	// Offcet

	VertexBufferBinding(ID3D11Buffer* buffer, int stride, int offset) {			// Constructor
		Buffer = buffer;
		Stride = stride;
		Offset = offset;
	}
};