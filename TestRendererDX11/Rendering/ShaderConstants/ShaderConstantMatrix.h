#pragma once
class ShaderConstantMatrix :
	public ShaderConstant 
{
protected:
	ShaderConstantMatrix(GraphicsDevice* device);
public:
	virtual void Set(DirectX::XMFLOAT4X4 matrix) = 0;
	virtual void Set(DirectX::XMFLOAT4X4& matrix) = 0;
	virtual void Set(DirectX::XMFLOAT4X4 matrix[], int count) = 0;
};