#pragma once
class ShaderConstantScalar :
	public ShaderConstant
{
protected:
	ShaderConstantScalar(GraphicsDevice* device);
public:
	virtual void Set(bool value) = 0;
	virtual void Set(int value) = 0;
	virtual void Set(float value) = 0;
	virtual void Set(float value[], int count) = 0;
};