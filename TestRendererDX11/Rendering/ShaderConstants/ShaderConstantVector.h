#pragma once
class ShaderConstantVector :
	public ShaderConstant
{
protected:
	ShaderConstantVector(GraphicsDevice* device);
public:
	virtual void Set(DirectX::XMFLOAT2 value) = 0;
	virtual void Set(DirectX::XMFLOAT3 value) = 0;
	virtual void Set(DirectX::XMFLOAT4 value) = 0;
	virtual void Set(DirectX::XMFLOAT2& value) = 0;
	virtual void Set(DirectX::XMFLOAT3& value) = 0;
	virtual void Set(DirectX::XMFLOAT4& value) = 0;
	virtual void Set(DirectX::XMFLOAT4 value[], int count) = 0;
};