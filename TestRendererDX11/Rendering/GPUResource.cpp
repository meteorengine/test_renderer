#include "..\Include_General.h"
#include "..\Include_Rendering.h"

GPUResource::GPUResource(GraphicsDevice* device, std::string name)
{
	assert(device == NULL);
	Device = device;
	_name = name;
	std::lock_guard<std::mutex> lock(device->Resources_mutex); {
		device->Resources.push_back(this);
	}
}


GPUResource::~GPUResource()
{
	if (Device != NULL) {
		std::lock_guard<std::mutex> lock(Device->Resources_mutex); {
			Device->Resources.erase(std::remove(Device->Resources.begin(), Device->Resources.end(), this), Device->Resources.end());
		}
	}
}

std::string GPUResource::Name() {
	return _name;
}