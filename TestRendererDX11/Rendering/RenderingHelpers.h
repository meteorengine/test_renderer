#pragma once
class RenderingHelpers																			// Class full of helper methods
{
private:
	static int SizeOfInBits(DXGI_FORMAT format);												// Size in bits of DXGI format
	static std::vector<int> FormatSizes;														// Cached sizes 
	static void InitFormats(std::vector<int> formats, int size);								// Create cache
public:
	static DXGI_FORMAT GetDXGIFormat(PIXEL_FORMAT format);										// Get DXGI format from Pixel format
	static int GetTextureMemoryUsage(DXGI_FORMAT format, int width, int height, int miLevels);	// Calculate approx. texture size
};

