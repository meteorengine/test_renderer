#pragma once
struct PixelFormatInfo																							// Informations about specific pixel format
{
	int BlockWidth;																								// Block width
	int BlockHeight;																							// Block height
	int BlockBytes;																								// Block bytes
	int NumComponents;																							// Number of components
	int PlatformFormat;																							// DXGI format
	bool IsSupported;																							// Is supported on current computer?
	PIXEL_FORMAT Format;																						// Format it's info is this

	PixelFormatInfo(PIXEL_FORMAT format, int blockWidth, int blockHeight, int blockBytes, int numComponents)	// Constructor
	{
		Format = format;
		BlockWidth = blockWidth;
		BlockHeight = blockHeight;
		BlockBytes = blockBytes;
		NumComponents = numComponents;
		IsSupported = true;
		PlatformFormat = 0;
	}
};