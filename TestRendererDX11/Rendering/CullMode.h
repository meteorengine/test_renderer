#pragma once
enum CULL_MODE			// Generic cull mode states for Rasterizer
{
	CULL_MODE_NORMAL,	// Normal cull mode
	CULL_MODE_INVERSED,	// Inversed cull mode
	CULL_MODE_TWOSIDED	// Twosided cull mode
};