#pragma once
enum DeviceState : byte	// State of GraphicsDevice
{
	Missing,			// Device is missing
	Created,			// Device is created
	Inited,				// Device is initialized
	Ready,				// Device is ready
	PreDisposing,		// Device is pre-disposing
	PreDisposed,		// Device is pre-disposed
	Disposing,			// Device is disposing
	Disposed			// Device is disposed
};