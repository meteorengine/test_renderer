#include "..\Include_General.h"
#include "..\Include_Rendering.h"

bool RenderingContext::IsMain() const {
	return _isMain;
}
int RenderingContext::Width() const {
	return _width;
}
int RenderingContext::Height() const {
	return _height;
}
bool RenderingContext::IsFullscreen() const {
	return _isFullscreen;
}
DirectX::XMINT2 RenderingContext::Size() const {
	DirectX::XMINT2 size;
	size.x = _width;
	size.y = _height;
	return size;
}
float RenderingContext::AspectRatio() const {
	return _width / (float)_height;
}
std::string RenderingContext::Name() const {
	return _name;
}

RenderingContext::RenderingContext(GraphicsDevice* device, HWND output, std::string name, bool main)
{
	Device = device;
	_isMain = main;
	_width = 0;
	_height = 0;
	_isFullscreen = false;
	_name = name;
	this->output = output;
}


RenderingContext::~RenderingContext()
{
	
}

bool RenderingContext::Resize(int width, int height, bool fullscreen) {
	_width = width;
	_height = height;
	_isFullscreen = fullscreen;
	//Pool alocate memory
	//Maybe call Resized?
	resized = true;
	return false;
}

void RenderingContext::drawGUI() {
	if (DrawGUI) {
		DrawGUI();
	}
}