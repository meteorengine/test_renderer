#pragma once
class ShaderConstantVectorDX11 :
	public ShaderConstantVector
{
private:
	ID3DX11EffectVectorVariable* _handle;
public:
	ShaderConstantVectorDX11(ShaderDX11 parent, ID3DX11EffectVectorVariable* handle);
	~ShaderConstantVectorDX11();
	void Set(DirectX::XMFLOAT2 value);
	void Set(DirectX::XMFLOAT3 value);
	void Set(DirectX::XMFLOAT4 value);
	void Set(DirectX::XMFLOAT2& value);
	void Set(DirectX::XMFLOAT3& value);
	void Set(DirectX::XMFLOAT4& value);
	void Set(DirectX::XMFLOAT4 value[], int count);
};

