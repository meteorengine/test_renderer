#pragma once
class ShaderConstantMatrixDX11 :
	public ShaderConstantMatrix
{
private:
	ID3DX11EffectMatrixVariable* _handle;
public:
	ShaderConstantMatrixDX11(ShaderDX11 parent, ID3DX11EffectMatrixVariable* handle);
	void Set(DirectX::XMFLOAT4X4 matrix);
	void Set(DirectX::XMFLOAT4X4& matrix);
	void Set(DirectX::XMFLOAT4X4 matrix[], int count);
	~ShaderConstantMatrixDX11();
};

