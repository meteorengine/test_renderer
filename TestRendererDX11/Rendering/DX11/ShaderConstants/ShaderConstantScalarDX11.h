#pragma once
class ShaderConstantScalarDX11:
	public ShaderConstantScalar
{
private:
	ID3DX11EffectScalarVariable* _handle;
public:
	ShaderConstantScalarDX11(ShaderDX11 parent, ID3DX11EffectScalarVariable* handle);
	~ShaderConstantScalarDX11();
	void Set(bool value);
	void Set(int value);
	void Set(float value);
	void Set(float value[], int count);
};

