#include "..\..\..\Include_General.h"
#include "..\..\..\Include_Rendering.h"
#include "..\..\..\Include_Rendering_DX11.h"

ShaderConstantVectorDX11::ShaderConstantVectorDX11(ShaderDX11 parent, ID3DX11EffectVectorVariable* handle) : ShaderConstantVector(parent.Device)
{
	_handle = handle;
}

ShaderConstantVectorDX11::~ShaderConstantVectorDX11()
{
	SAFE_RELEASE(_handle);
}

void ShaderConstantVectorDX11::Set(DirectX::XMFLOAT2 value) 
{
	_handle->SetFloatVector((float*)&value);
}

void ShaderConstantVectorDX11::Set(DirectX::XMFLOAT3 value) 
{
	_handle->SetFloatVector((float*)&value);
}

void ShaderConstantVectorDX11::Set(DirectX::XMFLOAT4 value) 
{
	_handle->SetFloatVector((float*)&value);
}

void ShaderConstantVectorDX11::Set(DirectX::XMFLOAT2& value) 
{
	_handle->SetFloatVector((float*)&value);
}

void ShaderConstantVectorDX11::Set(DirectX::XMFLOAT3& value) 
{
	_handle->SetFloatVector((float*)&value);
}

void ShaderConstantVectorDX11::Set(DirectX::XMFLOAT4& value) 
{
	_handle->SetFloatVector((float*)&value);
}

void ShaderConstantVectorDX11::Set(DirectX::XMFLOAT4 value[], int count) 
{
	_handle->SetFloatVectorArray((float*)&value, 0, count);
}