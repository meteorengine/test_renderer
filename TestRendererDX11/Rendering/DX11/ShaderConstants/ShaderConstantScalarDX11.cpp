#include "..\..\..\Include_General.h"
#include "..\..\..\Include_Rendering.h"
#include "..\..\..\Include_Rendering_DX11.h"

ShaderConstantScalarDX11::ShaderConstantScalarDX11(ShaderDX11 parent, ID3DX11EffectScalarVariable* handle) : ShaderConstantScalar(parent.Device)
{
	_handle = handle;
}

ShaderConstantScalarDX11::~ShaderConstantScalarDX11()
{
	SAFE_RELEASE(_handle);
}

void ShaderConstantScalarDX11::Set(bool value)
{
	_handle->SetBool(&value);
}

void ShaderConstantScalarDX11::Set(int value)
{
	_handle->SetInt(value);
}

void ShaderConstantScalarDX11::Set(float value) 
{
	_handle->SetFloat(value);
}

void ShaderConstantScalarDX11::Set(float value[], int count) 
{
	_handle->SetFloatArray(reinterpret_cast<float*>(&value), 0, count);
}