#include "..\..\..\Include_General.h"
#include "..\..\..\Include_Rendering.h"
#include "..\..\..\Include_Rendering_DX11.h"

ShaderConstantMatrixDX11::ShaderConstantMatrixDX11(ShaderDX11 parent, ID3DX11EffectMatrixVariable* handle) : ShaderConstantMatrix(parent.Device)
{
	_handle = handle;
}

void ShaderConstantMatrixDX11::Set(DirectX::XMFLOAT4X4 matrix)
{
	_handle->SetMatrix(reinterpret_cast<float*>(&matrix));
	Device->shaderConstantsBindCounter++;
}

void ShaderConstantMatrixDX11::Set(DirectX::XMFLOAT4X4& matrix)
{
	_handle->SetMatrix(reinterpret_cast<float*>(&matrix));
	Device->shaderConstantsBindCounter++;
}

void ShaderConstantMatrixDX11::Set(DirectX::XMFLOAT4X4 matrix[], int count)
{
	_handle->SetMatrixArray(reinterpret_cast<float*>(&matrix), 0, count);
	Device->shaderConstantsBindCounter++;
}

ShaderConstantMatrixDX11::~ShaderConstantMatrixDX11()
{
	SAFE_RELEASE(_handle);
}
