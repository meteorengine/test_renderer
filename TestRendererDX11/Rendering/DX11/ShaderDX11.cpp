#include "..\..\Include_General.h"
#include "..\..\Include_Rendering.h"
#include "..\..\Include_Rendering_DX11.h"

bool ShaderDX11::IsOptimized() {
	return _effect != NULL && _effect->IsOptimized();
}

void ShaderDX11::Load(std::string file) {
	release();

}

void ShaderDX11::CreateInputLayout(int index, INPUT_LAYOUT_TYPE type) {

}

void ShaderDX11::Apply(int techniqueIndex) {
}

void ShaderDX11::Optimize() {
	_effect->Optimize();
}

ShaderConstantScalar* ShaderDX11::GetScalarConstant(std::string name) { return NULL; }
ShaderConstantVector* ShaderDX11::GetVectorConstant(std::string name){ return NULL; }
ShaderConstantMatrix* ShaderDX11::GetMatrixConstant(std::string name){ return NULL; }

ShaderDX11::ShaderDX11(GraphicsDeviceDX11* device, std::string name) : Shader(device, name) {
	_device = device->Device;
}

ShaderDX11::~ShaderDX11()
{
}

void ShaderDX11::release() {

}