#include "..\..\Include_General.h"
#include "..\..\Include_Rendering.h"
#include "..\..\Include_Rendering_DX11.h"

GraphicsDeviceDX11::GraphicsDeviceDX11(HWND hWnd) : GraphicsDevice(hWnd)
{
	D3D11CreateDevice(GraphicsDevice::Adapter()->AdapterDXGI, D3D_DRIVER_TYPE_UNKNOWN, 0, D3D11_CREATE_DEVICE_BGRA_SUPPORT, NULL, 0, D3D11_SDK_VERSION, &Device, NULL, NULL);
	setupFormats();
	_currentContext = new RenderingContextDX11(this, hWnd, "Main Context", true);
	mainContext = _currentContext;

	//TODO: Load fullscreen from config????
	if (_currentContext->Resize(_initialWidth, _initialHeight, false)) {
		_currentContext = NULL;
		MessageBox(0, "Cannot create Main rendering Context", "Error", MB_OK);
	}
	if (Device->GetFeatureLevel() < D3D_FEATURE_LEVEL_11_0) {
		//Shouldn't happen but still;
		MessageBox(0, "Current GPU not supported", "Error", MB_OK);
	}
	_vsync = false; //TODO: Also load from config

	Device->GetImmediateContext(&_context);
}

GraphicsDeviceDX11::~GraphicsDeviceDX11()
{
}

void GraphicsDeviceDX11::Init() {
	if (_state != DeviceState::Created) {
		return;
	}
	_bindedRTsCount = 0;
	setContext(mainContext);
	//TODO: Setup and create profiler timers

	// -- Setup rasterizer states -- //

	D3D11_RASTERIZER_DESC rsDesc;
	rsDesc.CullMode = D3D11_CULL_BACK;
	rsDesc.FillMode = D3D11_FILL_SOLID;
	Device->CreateRasterizerState(&rsDesc, &_rasterizerStates[0]);

	rsDesc.CullMode = D3D11_CULL_FRONT;
	rsDesc.FillMode = D3D11_FILL_SOLID;
	Device->CreateRasterizerState(&rsDesc, &_rasterizerStates[1]);

	rsDesc.CullMode = D3D11_CULL_NONE;
	rsDesc.FillMode = D3D11_FILL_SOLID;
	Device->CreateRasterizerState(&rsDesc, &_rasterizerStates[2]);

	rsDesc.CullMode = D3D11_CULL_BACK;
	rsDesc.FillMode = D3D11_FILL_WIREFRAME;
	Device->CreateRasterizerState(&rsDesc, &_rasterizerStates[3]);

	rsDesc.CullMode = D3D11_CULL_FRONT;
	rsDesc.FillMode = D3D11_FILL_WIREFRAME;
	Device->CreateRasterizerState(&rsDesc, &_rasterizerStates[4]);
			
	rsDesc.CullMode = D3D11_CULL_NONE;
	rsDesc.FillMode = D3D11_FILL_WIREFRAME;
	Device->CreateRasterizerState(&rsDesc, &_rasterizerStates[5]);

	// -- Setup blend states -- //

	D3D11_BLEND_DESC blDesc;
	blDesc.RenderTarget[0].BlendEnable		= false;
	blDesc.RenderTarget[0].SrcBlend			= D3D11_BLEND_ONE;
	blDesc.RenderTarget[0].SrcBlendAlpha	= D3D11_BLEND_ONE;
	blDesc.RenderTarget[0].DestBlend		= D3D11_BLEND_ZERO;
	blDesc.RenderTarget[0].DestBlendAlpha	= D3D11_BLEND_ZERO;
	Device->CreateBlendState(&blDesc, &_blendStates[0]);

	blDesc.RenderTarget[0].BlendEnable		= true;
	blDesc.RenderTarget[0].SrcBlend			= D3D11_BLEND_SRC_ALPHA;
	blDesc.RenderTarget[0].SrcBlendAlpha	= D3D11_BLEND_SRC_ALPHA;
	blDesc.RenderTarget[0].DestBlend		= D3D11_BLEND_ONE;
	blDesc.RenderTarget[0].DestBlendAlpha	= D3D11_BLEND_ONE;
	Device->CreateBlendState(&blDesc, &_blendStates[1]);

	blDesc.RenderTarget[0].BlendEnable		= true;
	blDesc.RenderTarget[0].SrcBlend			= D3D11_BLEND_ONE;
	blDesc.RenderTarget[0].SrcBlendAlpha	= D3D11_BLEND_ONE;
	blDesc.RenderTarget[0].DestBlend		= D3D11_BLEND_INV_SRC_ALPHA;
	blDesc.RenderTarget[0].DestBlendAlpha	= D3D11_BLEND_INV_SRC_ALPHA;
	Device->CreateBlendState(&blDesc, &_blendStates[2]);

	blDesc.RenderTarget[0].BlendEnable		= true;
	blDesc.RenderTarget[0].SrcBlend			= D3D11_BLEND_SRC_ALPHA;
	blDesc.RenderTarget[0].SrcBlendAlpha	= D3D11_BLEND_SRC_ALPHA;
	blDesc.RenderTarget[0].DestBlend		= D3D11_BLEND_INV_SRC_ALPHA;
	blDesc.RenderTarget[0].DestBlendAlpha	= D3D11_BLEND_INV_SRC_ALPHA;
	Device->CreateBlendState(&blDesc, &_blendStates[3]);

	blDesc.RenderTarget[0].BlendEnable		= true;
	blDesc.RenderTarget[0].SrcBlend			= D3D11_BLEND_ZERO;
	blDesc.RenderTarget[0].SrcBlendAlpha	= D3D11_BLEND_ZERO;
	blDesc.RenderTarget[0].DestBlend		= D3D11_BLEND_INV_SRC_COLOR;
	blDesc.RenderTarget[0].DestBlendAlpha	= D3D11_BLEND_SRC_ALPHA;
	Device->CreateBlendState(&blDesc, &_blendStates[4]);

		
	// -- Setup depth stencil states -- //

	D3D11_DEPTH_STENCILOP_DESC dsopDesc;
	dsopDesc.StencilFunc = D3D11_COMPARISON_ALWAYS;
	dsopDesc.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
	dsopDesc.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsopDesc.StencilPassOp = D3D11_STENCIL_OP_KEEP;

	D3D11_DEPTH_STENCIL_DESC dsDesc;
	dsDesc.BackFace = dsopDesc;
	dsDesc.FrontFace = dsopDesc;
	dsDesc.DepthFunc = D3D11_COMPARISON_LESS;
	dsDesc.StencilEnable = false;
	dsDesc.StencilReadMask = 255;
	dsDesc.StencilWriteMask = 255;
	dsDesc.DepthEnable = true;
	dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;

	Device->CreateDepthStencilState(&dsDesc, &_depthStencilStates[0]);

	dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
	Device->CreateDepthStencilState(&dsDesc, &_depthStencilStates[1]);

	dsDesc.DepthEnable = false;
	dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
	Device->CreateDepthStencilState(&dsDesc, &_depthStencilStates[2]);

	// -- Prepare quad buffer -- //
	int stride = sizeof(DirectX::XMFLOAT3) + sizeof(DirectX::XMFLOAT2);
	int sizeInBytes = stride * 3;
	PrivateDeviceMemoryUsage += sizeInBytes;

	D3D11_BUFFER_DESC bDesc;
	bDesc.ByteWidth = sizeInBytes;
	bDesc.Usage = D3D11_USAGE_IMMUTABLE;
	bDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bDesc.CPUAccessFlags = 0;
	bDesc.MiscFlags = 0;
	bDesc.StructureByteStride = 0;

	float quad[] = {-1,	-1,	0, 0, 1, -1, 3, 0, 0, -1, 3, -1, 0, -2, 1};
	D3D11_SUBRESOURCE_DATA quadData;
	quadData.pSysMem = quad;

	Device->CreateBuffer(&bDesc, &quadData, &_quad_Buff);
	_quad_Bind = new VertexBufferBinding(_quad_Buff, stride, 0);

	_cullMode = CULL_MODE::CULL_MODE_NORMAL;

	_wireframe = true;
	SetWireframe(false);

	_blendState = BLEND_STATE_ADDITIVE;
	SetBlendState(BLEND_STATE_OPAQUE);

	_depthStencilState = DEPTH_STENCIL_STATE_NONE;
	SetDepthStencilState(DEPTH_STENCIL_STATE_DEFAULT);

	_state = DeviceState::Inited;
}

void GraphicsDeviceDX11::setupFormats() {
	//Check if required formats are availible
	checkFormatSupport(DXGI_FORMAT_R8G8B8A8_UNORM, D3D11_FORMAT_SUPPORT_MIP | D3D11_FORMAT_SUPPORT_RENDER_TARGET | D3D11_FORMAT_SUPPORT_DISPLAY, true);
	checkFormatSupport(DXGI_FORMAT_R16G16B16A16_FLOAT, D3D11_FORMAT_SUPPORT_MIP | D3D11_FORMAT_SUPPORT_RENDER_TARGET | D3D11_FORMAT_SUPPORT_BLENDABLE, true);
	checkFormatSupport(DXGI_FORMAT_R16_FLOAT, D3D11_FORMAT_SUPPORT_MIP | D3D11_FORMAT_SUPPORT_RENDER_TARGET | D3D11_FORMAT_SUPPORT_SHADER_GATHER, true);
	checkFormatSupport(DXGI_FORMAT_R16_UNORM, D3D11_FORMAT_SUPPORT_SHADER_SAMPLE_COMPARISON | D3D11_FORMAT_SUPPORT_MIP | D3D11_FORMAT_SUPPORT_RENDER_TARGET | D3D11_FORMAT_SUPPORT_SHADER_GATHER | D3D11_FORMAT_SUPPORT_SHADER_GATHER_COMPARISON, true);
	checkFormatSupport(DXGI_FORMAT_R32_FLOAT, D3D11_FORMAT_SUPPORT_MIP | D3D11_FORMAT_SUPPORT_RENDER_TARGET | D3D11_FORMAT_SUPPORT_SHADER_GATHER, true);
	checkFormatSupport(DXGI_FORMAT_R10G10B10A2_TYPELESS, D3D11_FORMAT_SUPPORT_MIP, true);
	checkFormatSupport(DXGI_FORMAT_R11G11B10_FLOAT, D3D11_FORMAT_SUPPORT_MIP | D3D11_FORMAT_SUPPORT_RENDER_TARGET, true);
	checkFormatSupport(DXGI_FORMAT_BC1_TYPELESS, D3D11_FORMAT_SUPPORT_MIP, true);
	checkFormatSupport(DXGI_FORMAT_BC2_TYPELESS, D3D11_FORMAT_SUPPORT_MIP, true);
	checkFormatSupport(DXGI_FORMAT_BC3_TYPELESS, D3D11_FORMAT_SUPPORT_MIP, true);
	checkFormatSupport(DXGI_FORMAT_BC4_TYPELESS, D3D11_FORMAT_SUPPORT_MIP, true);
	checkFormatSupport(DXGI_FORMAT_BC5_TYPELESS, D3D11_FORMAT_SUPPORT_MIP, true);
	checkFormatSupport(DXGI_FORMAT_D32_FLOAT, D3D11_FORMAT_SUPPORT_MIP | D3D11_FORMAT_SUPPORT_DEPTH_STENCIL, true);

	//Check if optional formats are availible
	Formats[1]->PlatformFormat	= DXGI_FORMAT_R8G8B8A8_UNORM;

	Formats[2]->PlatformFormat	= DXGI_FORMAT_R16G16B16A16_FLOAT;

	Formats[3]->PlatformFormat	= DXGI_FORMAT_R32G32B32A32_FLOAT;
	Formats[3]->IsSupported		= checkFormatSupport(DXGI_FORMAT_R32G32B32A32_FLOAT, D3D11_FORMAT_SUPPORT_MIP | D3D11_FORMAT_SUPPORT_RENDER_TARGET,false);

	Formats[4]->PlatformFormat	= DXGI_FORMAT_A8_UNORM;
	Formats[4]->IsSupported		= checkFormatSupport(DXGI_FORMAT_A8_UNORM, D3D11_FORMAT_SUPPORT_MIP | D3D11_FORMAT_SUPPORT_RENDER_TARGET | D3D11_FORMAT_SUPPORT_SHADER_GATHER, false);

	Formats[5]->PlatformFormat	= DXGI_FORMAT_R16_FLOAT;

	Formats[6]->PlatformFormat	= DXGI_FORMAT_R32_FLOAT;
	Formats[6]->IsSupported		= checkFormatSupport(DXGI_FORMAT_R32_FLOAT, D3D11_FORMAT_SUPPORT_MIP | D3D11_FORMAT_SUPPORT_RENDER_TARGET | D3D11_FORMAT_SUPPORT_SHADER_GATHER, false);

	Formats[7]->PlatformFormat	= DXGI_FORMAT_R16G16_FLOAT;
	Formats[7]->IsSupported		= checkFormatSupport(DXGI_FORMAT_R16G16_FLOAT, D3D11_FORMAT_SUPPORT_MIP | D3D11_FORMAT_SUPPORT_RENDER_TARGET, false);

	Formats[8]->PlatformFormat	= DXGI_FORMAT_R32G32_FLOAT;
	Formats[8]->IsSupported		= checkFormatSupport(DXGI_FORMAT_R32G32_FLOAT, D3D11_FORMAT_SUPPORT_MIP | D3D11_FORMAT_SUPPORT_RENDER_TARGET, false);

	Formats[9]->PlatformFormat	= DXGI_FORMAT_R10G10B10A2_UNORM;

	Formats[10]->PlatformFormat = DXGI_FORMAT_R11G11B10_FLOAT;

	Formats[11]->PlatformFormat = DXGI_FORMAT_BC1_UNORM;

	Formats[12]->PlatformFormat = DXGI_FORMAT_BC2_UNORM;

	Formats[13]->PlatformFormat = DXGI_FORMAT_BC3_UNORM;

	Formats[14]->PlatformFormat = DXGI_FORMAT_BC4_UNORM;

	Formats[15]->PlatformFormat = DXGI_FORMAT_BC5_UNORM;

	Formats[16]->PlatformFormat = DXGI_FORMAT_B8G8R8A8_UNORM;
	Formats[16]->IsSupported	= checkFormatSupport(DXGI_FORMAT_B8G8R8A8_UNORM, D3D11_FORMAT_SUPPORT_MIP | D3D11_FORMAT_SUPPORT_RENDER_TARGET | D3D11_FORMAT_SUPPORT_DISPLAY, false);

	Formats[17]->PlatformFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;

	Formats[18]->PlatformFormat = DXGI_FORMAT_R32_FLOAT;

	Formats[19]->PlatformFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
	Formats[19]->IsSupported	= checkFormatSupport(DXGI_FORMAT_D24_UNORM_S8_UINT, D3D11_FORMAT_SUPPORT_MIP | D3D11_FORMAT_SUPPORT_DEPTH_STENCIL, false);

	Formats[20]->PlatformFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;

	Formats[21]->PlatformFormat = DXGI_FORMAT_D32_FLOAT;

	Formats[22]->PlatformFormat = DXGI_FORMAT_R16_UNORM;

	Formats[23]->PlatformFormat = DXGI_FORMAT_B5G6R5_UNORM;
	Formats[23]->IsSupported	= checkFormatSupport(DXGI_FORMAT_B5G6R5_UNORM, D3D11_FORMAT_SUPPORT_MIP, false);
}

bool GraphicsDeviceDX11::checkFormatSupport(DXGI_FORMAT format, UINT flags, bool isNecessary) {
	HRESULT supported = Device->CheckFormatSupport(format, &flags);
	flags |= D3D11_FORMAT_SUPPORT_TEXTURE2D;
	if (isNecessary && FAILED(supported)) {
		MessageBox(0, "Format XXX is not supported on this platform.", "Error", MB_OK);
		//TODO: Exit game
	}
	return SUCCEEDED(supported);
}

RendererType GraphicsDeviceDX11::Renderer() const {
	return RendererType::DirectX_11;
}

bool GraphicsDeviceDX11::Wireframe() const {
	return _wireframe;
}

BLEND_STATE GraphicsDeviceDX11::BlendState() const {
	return _blendState;
}

DEPTH_STENCIL_STATE GraphicsDeviceDX11::DepthStencilState() const {
	return _depthStencilState;
}

CULL_MODE GraphicsDeviceDX11::CullMode() const {
	return _cullMode;
}

void GraphicsDeviceDX11::SetWireframe(bool wf) {
	if (wf != _wireframe) {
		_wireframe = wf;
		int state = (int)_cullMode;
		if (_wireframe) {
			state += 3;
		}
		_context->RSSetState(_rasterizerStates[state]);
	}
}

void GraphicsDeviceDX11::SetBlendState(BLEND_STATE bs) {
	if (bs != _blendState)
	{
		_blendState = bs;
		int state = (int)_blendState;
		_context->OMSetBlendState(_blendStates[state], NULL, -1);
	}
}
void GraphicsDeviceDX11::SetDepthStencilState(DEPTH_STENCIL_STATE dss) {
	if (dss != _depthStencilState)
	{
		_depthStencilState = dss;
		int state = (int)_depthStencilState;
		_context->OMSetDepthStencilState(_depthStencilStates[state], 0);
	}
}

void GraphicsDeviceDX11::SetCullMode(CULL_MODE cm) {
	if (cm != _cullMode) {
		_cullMode = cm;
		int state = (int)_cullMode;
		if (_wireframe) {
			state += 3;
		}
		_context->RSSetState(_rasterizerStates[state]);
	}
}
void GraphicsDeviceDX11::setContext(RenderingContext* context) {
	_currentContext = (RenderingContextDX11*)context;
	_currentBackBufferView = _currentContext->backBufferView;
	_currentDepthBufferView = _currentContext->depthBufferView;
	_bindedRTsCount = 0;
	_context->OMSetRenderTargets(1, &_currentBackBufferView, _currentDepthBufferView);
	_context->RSSetViewports(1, &_currentContext->viewport);
	_context->ClearDepthStencilView(_currentDepthBufferView, D3D11_CLEAR_DEPTH, 1, 0);
	_context->ClearRenderTargetView(_currentBackBufferView, new float[4] { 0,0,0,1 });
}

//THIS IS TEMP FUNCTION, JUST TO TEST
void GraphicsDeviceDX11::Draw() {
	setContext(mainContext);
	_currentContext->Draw2D();
	_currentContext->swapChain->Present(0, 0);
}