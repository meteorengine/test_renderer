#pragma once
class RenderingContextDX11 :																	// DX11 implementation of Rendering Context
	public RenderingContext
{
private:
	struct SwapChainEntry																		// Swap chain entry
	{
		IDXGISwapChain* swapChain;																// Swap chein
		ID3D11Texture2D* backBuffer;															// Back buffer
		ID3D11RenderTargetView* backBufferView;													// Back buffer view
	};
	std::map<HWND, SwapChainEntry> _swapChains;													// Collection of swapchains (cache)
	GraphicsDeviceDX11* _device;																// Graphics device
	int _memoryUsage;																			// Memory usage of rendering context (approx.)
	void release();																				// Release
public:
	IDXGISwapChain* swapChain;																	// Current swapchain
	ID3D11Texture2D* backBuffer;																// Current back buffer
	ID3D11RenderTargetView* backBufferView;														// Current back buffer view
	ID3D11Texture2D* depthBuffer;																// Current depth buffer
	ID3D11DepthStencilView* depthBufferView;													// Current depth buffer view
	D3D11_VIEWPORT viewport;																	// Current viewport
	ID2D1RenderTarget* RenderTarget2D;															// D2D1 render target
	RenderingContextDX11(GraphicsDevice* device, HWND output, std::string name, bool main);		// Constructor
	bool Resize(int width, int height, bool fullscreen);										// Resize context
	bool IsFullscreen() const;																	// Is fullscreen?
	void SetOutput(HWND handle);																// Set output
	void Draw2D();																				// Draw 2D
	~RenderingContextDX11();																	// Dispose
};

