#include "..\..\Include_General.h"
#include "..\..\Include_Rendering.h"
#include "..\..\Include_Rendering_DX11.h"


RenderingContextDX11::RenderingContextDX11(GraphicsDevice* device, HWND output, std::string name, bool main) : RenderingContext(device,output,name,main)
{
	_swapChains = std::map<HWND, SwapChainEntry>();
	_device = (GraphicsDeviceDX11*)device;
	_memoryUsage = 0;
}


RenderingContextDX11::~RenderingContextDX11()
{
	release();
	_device = NULL;
}

bool RenderingContextDX11::Resize(int width, int height, bool fullscreen) {
	if (fullscreen == IsFullscreen(), width == Width(), height == Height()) {
		return false;
	}
	if (fullscreen && !_device->Adapter()->PrimaryOutput) {
		if (width == Width() && height == Height()) {
			return false;
		}
		fullscreen = false;
	}
	Device->AddLocker();
	bool result;
	__try
	{
		release();
		D3D11_VIEWPORT vp;
		vp.TopLeftX = 0;
		vp.TopLeftY = 0;
		vp.Width = (FLOAT)width;
		vp.Height = (FLOAT)height;
		vp.MinDepth = 0;
		vp.MaxDepth = 1;
		viewport = vp;

		int textureMemoryUsage;
		if (output != 0) {
			DXGI_MODE_DESC bufferDesc;	

			bufferDesc.Width = width;
			bufferDesc.Height = height;
			bufferDesc.RefreshRate.Numerator = 60;
			bufferDesc.RefreshRate.Denominator = 1;
			bufferDesc.Format = RenderingHelpers::GetDXGIFormat(Device->DefaultBackBufferFormat());
			bufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
			bufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

			DXGI_SWAP_CHAIN_DESC swapChainDesc;

			swapChainDesc.BufferCount = 1;
			swapChainDesc.BufferDesc = bufferDesc;
			swapChainDesc.Windowed = TRUE;
			swapChainDesc.OutputWindow = output;
			swapChainDesc.SampleDesc.Count = 1;
			swapChainDesc.SampleDesc.Quality = 0;
			swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
			swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
			swapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

			GraphicsDevice::FactoryDXGI->CreateSwapChain(_device->Device, &swapChainDesc, &swapChain);
			if (IsMain() && fullscreen) {
				swapChain->ResizeTarget(&bufferDesc);
				BOOL fs;
				IDXGIOutput* dxgiout;
				swapChain->GetFullscreenState(&fs, &dxgiout);
				swapChain->SetFullscreenState(true, dxgiout);
				swapChain->ResizeBuffers(swapChainDesc.BufferCount, width, height, bufferDesc.Format, swapChainDesc.Flags);
			}

			textureMemoryUsage = RenderingHelpers::GetTextureMemoryUsage(bufferDesc.Format, bufferDesc.Width, bufferDesc.Height, 1);
			_memoryUsage += textureMemoryUsage;
			_device->PrivateDeviceMemoryUsage += textureMemoryUsage;

			swapChain->GetBuffer(0, __uuidof(backBuffer), reinterpret_cast<void**>(&backBuffer));
			_device->Device->CreateRenderTargetView(backBuffer, NULL, &backBufferView);
			IDXGISurface* surface;
			backBuffer->QueryInterface<IDXGISurface>(&surface);
			D2D1_RENDER_TARGET_PROPERTIES props = D2D1::RenderTargetProperties(D2D1_RENDER_TARGET_TYPE_DEFAULT, D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED));
			_device->FactoryDirect2D1->CreateDxgiSurfaceRenderTarget(surface, &props, &RenderTarget2D);
			SAFE_RELEASE(surface);
			RenderTarget2D->SetAntialiasMode(D2D1_ANTIALIAS_MODE_PER_PRIMITIVE);
			RenderTarget2D->SetTextAntialiasMode(D2D1_TEXT_ANTIALIAS_MODE_CLEARTYPE);
			SwapChainEntry schain;
			schain.backBuffer = backBuffer;
			schain.backBufferView = backBufferView;
			schain.swapChain = swapChain;
			_swapChains[output] = schain;
		}
		DXGI_SAMPLE_DESC dpSampleDesc;
		dpSampleDesc.Count = 1;
		dpSampleDesc.Quality = 0;

		D3D11_TEXTURE2D_DESC dpDesc;
		dpDesc.Format = RenderingHelpers::GetDXGIFormat(Device->DefaultDepthBufferFormat());
		dpDesc.ArraySize = 1;
		dpDesc.MipLevels = 1;
		dpDesc.Width = width;
		dpDesc.Height = height;
		dpDesc.SampleDesc = dpSampleDesc;
		dpDesc.Usage = D3D11_USAGE_DEFAULT;
		dpDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		dpDesc.CPUAccessFlags = 0;
		dpDesc.MiscFlags = 0;

		_device->Device->CreateTexture2D(&dpDesc, NULL, &depthBuffer);
		_device->Device->CreateDepthStencilView(depthBuffer, NULL, &depthBufferView);
		textureMemoryUsage = RenderingHelpers::GetTextureMemoryUsage(dpDesc.Format, dpDesc.Width, dpDesc.Height, 1);
		_memoryUsage += textureMemoryUsage;
		_device->PrivateDeviceMemoryUsage += textureMemoryUsage;
		result = RenderingContext::Resize(width, height, fullscreen);
	}
	__finally
	{
		Device->RemoveLocker();
	}

	return result;
}

bool RenderingContextDX11::IsFullscreen() const {
	if (!swapChain) {
		return false;
	}
	BOOL isFullscreen = false;
	swapChain->GetFullscreenState(&isFullscreen, NULL);
	return isFullscreen;
}

void RenderingContextDX11::release() {
	SAFE_RELEASE(depthBufferView);
	SAFE_RELEASE(depthBuffer);
	SAFE_RELEASE(RenderTarget2D);
	for (std::map<HWND, SwapChainEntry>::iterator it = _swapChains.begin(); it != _swapChains.end(); ++it)
	{
		SAFE_RELEASE(it->second.backBuffer);
		SAFE_RELEASE(it->second.backBufferView);
		SAFE_RELEASE(it->second.swapChain);
	}
	_swapChains.clear();
	backBufferView = NULL;
	backBuffer = NULL;
	swapChain = NULL;
	_device->PrivateDeviceMemoryUsage -= _memoryUsage;
	_memoryUsage = 0;
}

void RenderingContextDX11::SetOutput(HWND handle) {
	if (handle != output) {
		output = handle;
		if (output != 0) {
			
			if (_swapChains.find(output) == _swapChains.end()) {
				DXGI_MODE_DESC bufferDesc;

				bufferDesc.Width = Width();
				bufferDesc.Height = Height();
				bufferDesc.RefreshRate.Numerator = 60;
				bufferDesc.RefreshRate.Denominator = 1;
				bufferDesc.Format = RenderingHelpers::GetDXGIFormat(Device->DefaultBackBufferFormat());
				bufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
				bufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

				DXGI_SWAP_CHAIN_DESC swapChainDesc;

				swapChainDesc.BufferCount = 1;
				swapChainDesc.BufferDesc = bufferDesc;
				swapChainDesc.Windowed = TRUE;
				swapChainDesc.OutputWindow = output;
				swapChainDesc.SampleDesc.Count = 1;
				swapChainDesc.SampleDesc.Quality = 0;
				swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
				swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
				swapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

				GraphicsDevice::FactoryDXGI->CreateSwapChain(_device->Device, &swapChainDesc, &swapChain);
				
				int textureMemoryUsage = RenderingHelpers::GetTextureMemoryUsage(bufferDesc.Format, bufferDesc.Width, bufferDesc.Height, 1);
				_memoryUsage += textureMemoryUsage;
				_device->PrivateDeviceMemoryUsage += textureMemoryUsage;
				swapChain->GetBuffer(0, __uuidof(backBuffer), reinterpret_cast<void**>(&backBuffer));
				_device->Device->CreateRenderTargetView(backBuffer, NULL, &backBufferView);
				IDXGISurface* surface;
				backBuffer->QueryInterface<IDXGISurface>(&surface);
				D2D1_RENDER_TARGET_PROPERTIES props = D2D1::RenderTargetProperties(D2D1_RENDER_TARGET_TYPE_DEFAULT, D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED));
				_device->FactoryDirect2D1->CreateDxgiSurfaceRenderTarget(surface, &props, &RenderTarget2D);
				SAFE_RELEASE(surface);
				RenderTarget2D->SetAntialiasMode(D2D1_ANTIALIAS_MODE_PER_PRIMITIVE);
				RenderTarget2D->SetTextAntialiasMode(D2D1_TEXT_ANTIALIAS_MODE_CLEARTYPE);
				SwapChainEntry schain;
				schain.backBuffer = backBuffer;
				schain.backBufferView = backBufferView;
				schain.swapChain = swapChain;
				_swapChains[output] = schain;
			}
			else {
				SwapChainEntry schain = _swapChains[output];
				backBuffer = schain.backBuffer;
				backBufferView = schain.backBufferView;
				swapChain = schain.swapChain;
			}

		}
	}
}

void RenderingContextDX11::Draw2D() {
	RenderTarget2D->BeginDraw();
	__try
	{
		drawGUI();
	}
	__finally
	{
		RenderTarget2D->EndDraw();
	}
}