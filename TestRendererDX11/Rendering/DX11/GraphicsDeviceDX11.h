#pragma once

class GraphicsDeviceDX11 :																// DX11 Implementation of Graphics device
	public GraphicsDevice
{
private:
	RenderingContextDX11* _currentContext;												// Main context
	bool _vsync;																		// Use Vsync?
	ID3D11DeviceContext* _context;														// Device immidiate context
	ID3D11RasterizerState* _rasterizerStates[6];										// Rasterizer states
	ID3D11BlendState* _blendStates[5];													// Blend states
	ID3D11DepthStencilState* _depthStencilStates[3];									// Depth stencil states
	ID3D11Buffer* _quad_Buff;															// Quad buffer
	VertexBufferBinding* _quad_Bind;													// Quad binding
	CULL_MODE _cullMode;																// Current cull mode
	bool _wireframe;																	// Wireframe
	BLEND_STATE _blendState;															// Current blend state
	DEPTH_STENCIL_STATE _depthStencilState;												// Current epth stencil state
	int _bindedRTsCount;																// Count of binded Render Targets
	ID3D11RenderTargetView* _bindedRTs[4];												// List of binded Render targets
	ID3D11RenderTargetView* _currentBackBufferView;										// Current back buffer
	ID3D11DepthStencilView* _currentDepthBufferView;									// Current depth buffer
	void setContext(RenderingContext* context);											// Set rendering context
	void setupFormats();																// Setup formats
	bool checkFormatSupport(DXGI_FORMAT format, UINT flags, bool isNecessary);			// Check if DXGI format is supported
public:
	ID3D11Device* Device;																// D3D11 Device

	GraphicsDeviceDX11(HWND hWnd);														// Constructor
	~GraphicsDeviceDX11();																// Dispose

	void Init();																		// Initialize
	
	bool Wireframe() const;																// Get wireframe state
	BLEND_STATE BlendState() const;														// Get blend state
	DEPTH_STENCIL_STATE DepthStencilState() const;										// Get depth stencil state
	CULL_MODE CullMode() const;															// Get cull mode

	void SetWireframe(bool wf);															// Set wireframe
	void SetBlendState(BLEND_STATE bs);													// Set blend state
	void SetDepthStencilState(DEPTH_STENCIL_STATE dss);									// Set depth stencil state
	void SetCullMode(CULL_MODE cm);														// Set cull mode
	void Draw();																		// Draw	
	RendererType Renderer() const;														// Get current renderer type
};

