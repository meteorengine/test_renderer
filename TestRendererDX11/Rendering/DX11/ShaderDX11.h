#pragma once
class ShaderDX11 :
	public Shader
{
private:
	ID3D11Device* _device;
	ID3DX11Effect* _effect;
	void release();
public:
	~ShaderDX11();
	bool IsOptimized();
	void Load(std::string file);
	void CreateInputLayout(int index, INPUT_LAYOUT_TYPE type);
	void Apply(int techniqueIndex);
	void Optimize();
	ShaderConstantScalar* GetScalarConstant(std::string name);
	ShaderConstantVector* GetVectorConstant(std::string name);
	ShaderConstantMatrix* GetMatrixConstant(std::string name);
protected:
	ShaderDX11(GraphicsDeviceDX11* device, std::string name);
};

