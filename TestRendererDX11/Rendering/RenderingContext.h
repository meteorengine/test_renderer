#pragma once

class RenderingContext																	// Class containing Rener targets
{
private:
	bool _isMain;																		// Is main rendering context?
	bool _isFullscreen;																	// Is fullscreen?
	int _width;																			// Width
	int _height;																		// Height
	std::string _name;																	// Name of context

public:
	bool resized;																		// Resized?
	HWND output;																		// Target output
	GraphicsDevice* Device;																// Graphics device
	std::function<void()> DrawGUI;														// Called when drawing UI
	bool IsMain() const;																// Is main?
	int Width() const;																	// Width
	int Height() const;																	// Height
	virtual bool IsFullscreen() const;													// Is fullscreen?
	DirectX::XMINT2 Size() const;														// Size of context
	float AspectRatio() const;															// Aspect ratio of context
	std::string Name() const;															// Name of context

	RenderingContext(GraphicsDevice* device, HWND output, std::string name, bool main);	// Constructor
	virtual bool Resize(int width, int height, bool fullscreen);						// Resize context
	virtual void SetOutput(HWND handle) = 0;											// Set context output
	void drawGUI();																		// Invoke DrawGUI function
	~RenderingContext();																// Dispose
};

