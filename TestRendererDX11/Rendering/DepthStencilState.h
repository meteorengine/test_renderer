#pragma once
enum DEPTH_STENCIL_STATE			//Generic depth stencil states for Output Merger
{
	DEPTH_STENCIL_STATE_DEFAULT,	//Default depth stencil state
	DEPTH_STENCIL_STATE_READ,		//Read depth stencil state
	DEPTH_STENCIL_STATE_NONE		//None depth stencil state
};