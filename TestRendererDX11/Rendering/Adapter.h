#pragma once

class Adapter																		// Class containing methods for working with DXGI adapter
{
private:
	static Adapter* _bestAdapter;													// Best found adapter (NULL if not found yet)
	static std::vector<IDXGIAdapter*> _adapters;									// Collection of all avalible DXGI Adapters
	AdapterInfo _info;																// Info about current adapter
	int _index;																		// Current adapter's index (if index is 0 than it's primary adaper, to check this use IsPrimary())
	bool _isPrimary;																// Is adapter primary adapter?
	Adapter(IDXGIAdapter* adapter, bool isPrimary, int index);						// Creates new instance of Adapter class
	static bool sortAdapters(IDXGIAdapter* left, IDXGIAdapter* right);				// Sorts adapter using it's internal rules
	static D3D_FEATURE_LEVEL GetSupportedFeatureLevel(IDXGIAdapter* adapter);		// Gets maximum supported DirectX version, for this adapter
	static int getAdapterIndex(IDXGIAdapter* adapter);								// Gets index of current adapter in _adapters array 
public:
	IDXGIAdapter* AdapterDXGI;														// Corresponding DXGI adapter for this instance
	IDXGIOutput* PrimaryOutput;														// Primary output of DXGI adapter
	bool IsPrimary() const;															// Is adapter primary?
	int Index() const;																// Adapter's index
	AdapterInfo Info() const;														// Get informations about adapters
	bool IsDefaultAdapter() const;													// Is default adapter?
	D3D_FEATURE_LEVEL FeatureLevel() const;											// Feature level suported by this adapter
	std::wstring Description() const;												// Name of adapter (usually name of GPU)
	~Adapter();																		// Desctructor
	static Adapter* FindBest();														// Find best adapter on current computer
};