#pragma once
struct AdapterInfo {					// Contains informations about DXGI adapter
	std::wstring Description;			// Description of DXGI adapter (usually GPU name)
	UINT DeviceId;						// Device ID
	LUID Luid;							// Luid
	UINT Revision;						// Revision
	UINT SubsystemId;					// Subsystem ID
	UINT VendorId;						// Vendor ID
	SIZE_T DedicatedSystemMemory;		// Dedicated system memory amount
	SIZE_T DedicatedVideoMemory;		// Dedicated video memory amount
	SIZE_T SharedSystemMemory;			// Shared system memory amount

	bool IsAMD() const {				// Is AMD?
		return VendorId == 4096;
	};
	bool IsNVidia() const {				// Is nVidia?
		return VendorId == 4318;
	};
	bool IsIntel() const {				// Is Intel?
		return VendorId == 32902;
	};
};