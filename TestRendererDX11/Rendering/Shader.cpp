#include "..\Include_General.h"
#include "..\Include_Rendering.h"

Shader::Shader(GraphicsDevice* device, std::string name) : GPUResource(device, name) {
	_memoryUsage = 0;
}


Shader::~Shader()
{
}

int Shader::MemoryUsage() {
	return _memoryUsage;
}

bool Shader::IsLoaded() {
	return _memoryUsage > 0;
}

bool Shader::IsFullyLoaded() {
	return _memoryUsage > 0;
}
