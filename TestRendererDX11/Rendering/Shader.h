#pragma once
class Shader :
	public GPUResource
{
public:
	~Shader();
	int MemoryUsage();
	bool IsLoaded();
	bool IsFullyLoaded();
	virtual bool IsOptimized() = 0;
	static bool ForceRecompileAllShaders;
	virtual void Load(std::string file) = 0;
	virtual void CreateInputLayout(int index, INPUT_LAYOUT_TYPE type) = 0;
	virtual void Apply(int techniqueIndex) = 0;
	virtual void Optimize() = 0;
	virtual ShaderConstantScalar* GetScalarConstant(std::string name) = 0;
	virtual ShaderConstantVector* GetVectorConstant(std::string name) = 0;
	virtual ShaderConstantMatrix* GetMatrixConstant(std::string name) = 0;
protected:
	int _memoryUsage;
	std::map<std::string, ShaderConstant> _constants;
	Shader(GraphicsDevice* device, std::string name);
};

