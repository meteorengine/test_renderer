#include "..\Include_General.h"
#include "..\Include_Rendering.h"

IDXGIFactory1* GraphicsDevice::FactoryDXGI = NULL;
PixelFormatInfo* GraphicsDevice::Formats[24] = {
	new PixelFormatInfo(PIXEL_FORMAT_UNKNOWN, 0, 0, 0, 0),
	new PixelFormatInfo(PIXEL_FORMAT_R8G8B8A8, 1, 1, 4, 4),
	new PixelFormatInfo(PIXEL_FORMAT_R16G16B16A16, 1, 1, 8, 4),
	new PixelFormatInfo(PIXEL_FORMAT_R32G32B32A32, 1, 1, 16, 4),
	new PixelFormatInfo(PIXEL_FORMAT_A8, 1, 1, 1, 1),
	new PixelFormatInfo(PIXEL_FORMAT_R16, 1, 1, 2, 1),
	new PixelFormatInfo(PIXEL_FORMAT_R32, 1, 1, 4, 1),
	new PixelFormatInfo(PIXEL_FORMAT_R16G16, 1, 1, 4, 2),
	new PixelFormatInfo(PIXEL_FORMAT_R32G32, 1, 1, 8, 2),
	new PixelFormatInfo(PIXEL_FORMAT_R10G10B10A2, 1, 1, 4, 4),
	new PixelFormatInfo(PIXEL_FORMAT_R11G11B10, 1, 1, 4, 3),
	new PixelFormatInfo(PIXEL_FORMAT_BC1, 4, 4, 8, 3),
	new PixelFormatInfo(PIXEL_FORMAT_BC2, 4, 4, 16, 4),
	new PixelFormatInfo(PIXEL_FORMAT_BC3, 4, 4, 16, 4),
	new PixelFormatInfo(PIXEL_FORMAT_BC4, 4, 4, 16, 2),
	new PixelFormatInfo(PIXEL_FORMAT_BC5, 4, 4, 8, 1),
	new PixelFormatInfo(PIXEL_FORMAT_B8G8R8A8, 1, 1, 4, 4),
	new PixelFormatInfo(PIXEL_FORMAT_DEPTH_STENCIL, 1, 1, 4, 2),
	new PixelFormatInfo(PIXEL_FORMAT_SHADOW_DEPTH, 1, 1, 4, 1),
	new PixelFormatInfo(PIXEL_FORMAT_D16, 1, 1, 2, 1),
	new PixelFormatInfo(PIXEL_FORMAT_D24, 1, 1, 4, 1),
	new PixelFormatInfo(PIXEL_FORMAT_D32, 1, 1, 4, 1),
	new PixelFormatInfo(PIXEL_FORMAT_R16F, 1, 1, 2, 1),
	new PixelFormatInfo(PIXEL_FORMAT_B5G6R5, 1, 1, 2, 1)
};
GraphicsDevice::GraphicsDevice(HWND hWnd)
{
	_state = DeviceState::Created;

	D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &FactoryDirect2D1);
	DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(IDWriteFactory), reinterpret_cast<IUnknown**>(&FactoryDirectWrite));
	_adapter = Adapter::FindBest();

	//TODO: Load from config????
	_initialWidth = 1280;
	_initialHeight = 720;
}


GraphicsDevice::~GraphicsDevice()
{
	SAFE_RELEASE(FactoryDirect2D1);
	SAFE_RELEASE(FactoryDirectWrite);
}

RendererType GraphicsDevice::Renderer() const {
	return (RendererType)NULL;
}

Adapter* GraphicsDevice::Adapter() const{
	return _adapter;
}

DeviceState GraphicsDevice::State() const {
	return _state;
}

RendererType GraphicsDevice::CheckSupportedDirectX() {

	D3D_FEATURE_LEVEL featureLevel = Adapter::FindBest()->FeatureLevel();

	if (featureLevel >= D3D_FEATURE_LEVEL_11_0) {
		return RendererType::DirectX_11;
	}
	else if (featureLevel >= D3D_FEATURE_LEVEL_10_0) {
		return RendererType::DirectX_10;
	}

	return (RendererType)NULL;
}

PIXEL_FORMAT GraphicsDevice::DefaultBackBufferFormat() {
	return PIXEL_FORMAT_R8G8B8A8;
}

PIXEL_FORMAT GraphicsDevice::DefaultDepthBufferFormat() {
	return PIXEL_FORMAT_D32;
}

void GraphicsDevice::AddLocker() {
	_deviceLocker.lock();
}

void GraphicsDevice::AddLocker(int timeout) {
	_deviceLocker.try_lock_for(std::chrono::milliseconds(timeout));
}


void GraphicsDevice::RemoveLocker() {
	_deviceLocker.unlock();
}