#pragma once
enum BLEND_STATE					//Generic blend states for Output Merger
{
	BLEND_STATE_OPAQUE,				// Opaque blend state
	BLEND_STATE_ADDITIVE,			// Additive blend state
	BLEND_STATE_ALPHA_BLEND,		// Alpha blend blend state
	BLEND_STATE_NON_PREMULTIPLIED,	// Non premultiplied blend state
	BLEND_STATE_MULTIPLY			// Multiply blend state
};