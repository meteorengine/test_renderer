#pragma once

class GraphicsDevice												// Base class for whole renderer, contains master DirectX device 
{
private:
	Adapter* _adapter;												// Currently used adapter
protected:
	int _initialWidth;												// Initial Width
	int _initialHeight;												// Initial Height
	DeviceState _state;												// Current device state
	std::timed_mutex _deviceLocker;									// Device locker
public:
	std::vector<GPUResource*> Resources;							// Map of GPU Resources
	std::mutex Resources_mutex;										// Mutex for Resources vector
	static IDXGIFactory1* FactoryDXGI;								// DXGI Factory
	ID2D1Factory* FactoryDirect2D1;									// D2D Factory
	IDWriteFactory* FactoryDirectWrite;								// DirectWrite Factory
	RenderingContext* mainContext;									// Main Rendering context

	static PixelFormatInfo* Formats[24];							// Collection of (un)supported formats for DXGI
	int shaderConstantsBindCounter;									// Count of binded shader constants

	GraphicsDevice(HWND hWnd);										// Create new GraphicsDevice
	~GraphicsDevice();												// Dispose

	long PrivateDeviceMemoryUsage;									// Graphics memory usage by this device and it's rendering contexts

	RendererType Renderer() const;									// Renderer version
	Adapter* Adapter() const;										// Currently used adapter
	DeviceState State() const;										// Current device state
	
	virtual PIXEL_FORMAT DefaultBackBufferFormat();					// Default format used for Back buffer(s)
	virtual PIXEL_FORMAT DefaultDepthBufferFormat();				// Default format used for Depth buffer(s)

	virtual bool Wireframe() const = 0;								// Is in wireframe mode?
	virtual BLEND_STATE BlendState() const = 0;						// Get current blend state
	virtual DEPTH_STENCIL_STATE DepthStencilState() const = 0;		// Get current depth stencil state
	virtual CULL_MODE CullMode() const = 0;							// Get current cull mode

	virtual void SetWireframe(bool wf) = 0;							// Set wireframe mode
	virtual void SetBlendState(BLEND_STATE bs) = 0;					// Set blend sate
	virtual void SetDepthStencilState(DEPTH_STENCIL_STATE dss) = 0;	// Set depth stencil state
	virtual void SetCullMode(CULL_MODE cm) = 0;						// Set cull mode
	virtual void Init() = 0;										// Initialize the device

	void AddLocker();												// Add device locker
	void AddLocker(int timeout);									// Add timed device locker
	void RemoveLocker();											// Remove device locker

	static RendererType CheckSupportedDirectX();					// Get maximum supported version of DirectX on current computer
};

