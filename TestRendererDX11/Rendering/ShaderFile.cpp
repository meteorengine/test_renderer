#include "..\Include_General.h"
#include "..\Include_Rendering.h"

int ShaderFile::Password[] = { 176, 33, 90, 91, 185, 140, 212, 67, 113, 78, 111 };
ShaderFile::ShaderFile()
{
}

ShaderFile::ShaderFile(std::string path)
{
	UnPack(path);
}
ShaderFile::ShaderFile(std::istream& stream)
{
	UnPack(stream);
}

void ShaderFile::Pack(std::string path) {
	Pack(std::ofstream(path, std::ios::binary));
}

void ShaderFile::Pack(std::ostream& stream) {
	char header[3];
	header[0] = 83;
	header[1] = 70;
	header[2] = 0;
	stream.write(header, 3);
	MySerialization::Write(stream, 8);
	MySerialization::Write(stream, MaterialVersion);
	MySerialization::Write(stream, DateTime::Now());
	MySerialization::Write(stream, SourceEdited);
	MySerialization::Write(stream, DX10CacheEdited);
	MySerialization::Write(stream, DX11CacheEdited);
	if (HasSource()) {
		z_stream zs;
		memset(&zs, 0, sizeof(zs));
		if (deflateInit2(&zs, Z_BEST_COMPRESSION, Z_DEFLATED, 16 + MAX_WBITS, 8, Z_DEFAULT_STRATEGY) != Z_OK)
			MessageBox(NULL, "Error when compressing source", "", 0);

		zs.next_in = (Bytef *)&Source[0];
		zs.avail_in = Source.size();
		int ret;
		char outbuffer[4096];
		std::string compressedSource;
		do {
			zs.next_out = reinterpret_cast<Bytef*>(outbuffer);
			zs.avail_out = sizeof(outbuffer);

			ret = deflate(&zs, Z_FINISH);

			if (compressedSource.size() < zs.total_out) {
				compressedSource.append(outbuffer,
					zs.total_out - compressedSource.size());
			}

		} while (ret == Z_OK);

		deflateEnd(&zs);

		if (ret != Z_STREAM_END) {
			MessageBox(NULL, "Error when compressing source", "", 0);
		}
		MySerialization::Write(stream, (int)compressedSource.size());
		stream.write(compressedSource.c_str(), compressedSource.size());
	}
	else {
		MySerialization::Write(stream, 0);
	}
	MySerialization::Write(stream, IsMaterial);
	if (IsMaterial) {
		//TODO: Add support for materials
	}

	MySerialization::Write(stream, (int)DX10Cache.size());
	if (DX10Cache.size() > 0) {
		stream.write((char*)&DX10Cache[0], DX10Cache.size());
	}
	MySerialization::Write(stream, (int)DX11Cache.size());
	if (DX11Cache.size() > 0) {
		stream.write((char*)&DX11Cache[0], DX11Cache.size());
	}
	MySerialization::WriteByte(stream,126);
}

void ShaderFile::UnPack(std::string path)
{
	UnPack(std::ifstream(path, std::ios::binary));
}

void ShaderFile::UnPack(std::istream&  stream)
{
	Clear();
	char header[3];
	stream.read((char*)header, 3);
	if (header[0] != 83 || header[1] != 70 || header[2] != 0)
	{
		MessageBox(NULL, "Invalid Shader File.", "Error - Loading Shader File", 0);
		return;
	}
	int version = MySerialization::ReadInt(stream);
	switch (version)
	{
		//Dropped support for versions 4,5,6,7 wen translating code to C++... too much work with no result (nobody will use old Meteor v1 alpha version from 2013 with this) :D 
	case 8:
	{
		MaterialVersion = MySerialization::ReadInt(stream);
		CreationTime = MySerialization::ReadDateTime(stream);
		SourceEdited = MySerialization::ReadDateTime(stream);
		DX10CacheEdited = MySerialization::ReadDateTime(stream);
		DX11CacheEdited = MySerialization::ReadDateTime(stream);
		int sourceLength = MySerialization::ReadInt(stream);
		if (sourceLength > 0) {
			std::vector<unsigned char> gZippedSource;
			gZippedSource.resize(sourceLength);
			stream.read(reinterpret_cast<char*>(&gZippedSource[0]), sourceLength);


			z_stream zs; 
			memset(&zs, 0, sizeof(zs));
			if (inflateInit2(&zs, (16 + MAX_WBITS)) != Z_OK)
				MessageBox(NULL, "Error when decompressing source", "", 0);

			zs.next_in = (Bytef *)&gZippedSource[0];
			zs.avail_in = gZippedSource.size();

			int ret;
			char outbuffer[4096];
			do {
				zs.next_out = reinterpret_cast<Bytef*>(outbuffer);
				zs.avail_out = sizeof(outbuffer);

				ret = inflate(&zs, 0);

				if (Source.size() < zs.total_out) {
					Source.append(outbuffer,
						zs.total_out - Source.size());
				}

			} while (ret == Z_OK);

			inflateEnd(&zs);

			if (ret != Z_STREAM_END) {
				MessageBox(NULL, "Error when decompressing source", "", 0);
			}
		}
		IsMaterial = MySerialization::ReadBool(stream);
		if (IsMaterial) {
			//Read Material info
			//Read Material params
			MessageBox(NULL, "Add Material params deserialization", "", 0);
			int visjectSurfaceLength = MySerialization::ReadInt(stream);
			if (visjectSurfaceLength > 0)
			{
				VisjectSurface.resize(visjectSurfaceLength);
				stream.read(&VisjectSurface[0], visjectSurfaceLength);
			}
		}
		int cacheSizeDX10 = MySerialization::ReadInt(stream);
		if (cacheSizeDX10 > 0)
		{
			DX10Cache.resize(cacheSizeDX10);
			stream.read(&DX10Cache[0], cacheSizeDX10);
		}
		int cacheSizeDX11 = MySerialization::ReadInt(stream);
		if (cacheSizeDX11 > 0)
		{
			DX11Cache.resize(cacheSizeDX11);
			stream.read(&DX11Cache[0], cacheSizeDX11);
		}
	}
	break;
	default:
		std::string msg = "Unsupported Shader File version: ";
		msg += version;
		MessageBox(NULL, msg.c_str(), "Error - Loading Shader File", 0);
		break;
	}
	if (MySerialization::ReadByte(stream) != 126) {
		MessageBox(NULL, "Shader File is corrupted", "Error - Loading Shader File", 0);
	}
	IsDraft = false;
}

ShaderFile::~ShaderFile()
{
	Clear();
}

void ShaderFile::Clear() {
	Source.clear();
	IsMaterial = false;
	VisjectSurface.clear();
	DX10Cache.clear();
	DX11Cache.clear();
	//Clear material params
}

bool ShaderFile::HasSource() {
	return Source.size() > 0 ? true : false;
}