#pragma once
class ShaderFile
{
private:
	static int Password[];
	const int Version = 8;
public:
	int MaterialVersion;
	bool IsDraft;
	DateTime* CreationTime;
	DateTime* SourceEdited;
	DateTime* DX10CacheEdited;
	DateTime* DX11CacheEdited;
	std::string Source;
	bool IsMaterial;
	std::vector<char> VisjectSurface;
	std::vector<char> DX10Cache;
	std::vector<char> DX11Cache;

	ShaderFile();
	ShaderFile(std::string path);
	ShaderFile(std::istream& stream);
	~ShaderFile();
	void Pack(std::string path);
	void Pack(std::ostream& stream);
	void UnPack(std::string path);
	void UnPack(std::istream& stream);
	void Clear();
	bool HasSource();
};

