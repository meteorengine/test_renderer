#include "..\Include_General.h"
#include "..\Include_Rendering.h"

Adapter* Adapter::_bestAdapter = NULL;
std::vector<IDXGIAdapter*> Adapter::_adapters;

bool Adapter::IsPrimary() const {
	return _isPrimary;
}

int Adapter::Index() const {
	return _index;
}

AdapterInfo Adapter::Info() const {
	return _info;
}

bool Adapter::IsDefaultAdapter() const {
	return _index == 0;
}

D3D_FEATURE_LEVEL Adapter::FeatureLevel() const {
	return GetSupportedFeatureLevel(AdapterDXGI);
}

Adapter::Adapter(IDXGIAdapter* adapter, bool isPrimary, int index) {
	assert(index >= 0); //Index must be higher than zero
	assert(adapter != NULL); //Adapter cannot be null

	AdapterDXGI = adapter;
	_index = index;
	_isPrimary = isPrimary;

	IDXGIOutput* output;
	if(AdapterDXGI->EnumOutputs(0, &output) != DXGI_ERROR_NOT_FOUND) {
		PrimaryOutput = output;
	}
	DXGI_ADAPTER_DESC description;
	AdapterDXGI->GetDesc(&description);

	_info.Description = description.Description;
	_info.DeviceId = description.DeviceId;
	_info.Luid = description.AdapterLuid;
	_info.Revision = description.Revision;
	_info.SubsystemId = description.SubSysId;
	_info.VendorId = description.VendorId;
	_info.SharedSystemMemory = description.SharedSystemMemory;
	_info.DedicatedSystemMemory = description.DedicatedSystemMemory;
	_info.DedicatedVideoMemory = description.DedicatedVideoMemory;
}

std::wstring Adapter::Description() const {
	return _info.Description;
}

Adapter::~Adapter()
{
	SAFE_RELEASE(AdapterDXGI);
	PrimaryOutput = NULL;
	_index = -1;
}

Adapter* Adapter::FindBest() {
	if (GraphicsDevice::FactoryDXGI == NULL) {
		IDXGIFactory1* dxgiFactory;
		CreateDXGIFactory1(IID_PPV_ARGS(&dxgiFactory));
		GraphicsDevice::FactoryDXGI = dxgiFactory;
	}
	if (_bestAdapter == NULL) {
		int adapterIndex = 0;
		IDXGIAdapter1* adapter;
		_adapters.clear();
		while (GraphicsDevice::FactoryDXGI->EnumAdapters1(adapterIndex, &adapter) != DXGI_ERROR_NOT_FOUND)
		{
			DXGI_ADAPTER_DESC1 desc;
			adapter->GetDesc1(&desc);

			if (desc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE)
			{
				adapterIndex++;
				continue;
			}
			auto it = _adapters.begin();
			_adapters.insert(it, adapter);
			adapterIndex++;
		}

		if (_adapters.size() == 1) {
			_bestAdapter = new Adapter(_adapters[0], true, 0);
		}
		else {
			std::vector<IDXGIAdapter*> adapters = _adapters;
			std::sort(std::begin(adapters), std::end(adapters), sortAdapters);
			_bestAdapter = new Adapter(adapters[0], adapters[0] == _adapters[0], Adapter::getAdapterIndex(adapters[0]));
		}
	}
	return _bestAdapter;
}

D3D_FEATURE_LEVEL Adapter::GetSupportedFeatureLevel(IDXGIAdapter* adapter) {
	D3D_FEATURE_LEVEL MaxSupportedFeatureLevel; 
	D3D_FEATURE_LEVEL FeatureLevels[] = {
		//D3D_FEATURE_LEVEL_12_1,
		//D3D_FEATURE_LEVEL_12_0,
		//D3D_FEATURE_LEVEL_11_1,
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0
	};
	D3D11CreateDevice(adapter, D3D_DRIVER_TYPE_UNKNOWN, NULL, 0, FeatureLevels, ARRAYSIZE(FeatureLevels), D3D11_SDK_VERSION, NULL, &MaxSupportedFeatureLevel, NULL);
	return MaxSupportedFeatureLevel;
}

bool Adapter::sortAdapters(IDXGIAdapter* left, IDXGIAdapter* right) {
	DXGI_ADAPTER_DESC descLeft;
	DXGI_ADAPTER_DESC descRight;
	left->GetDesc(&descLeft);
	right->GetDesc(&descRight);

	D3D_FEATURE_LEVEL flLeft = Adapter::GetSupportedFeatureLevel(left);
	D3D_FEATURE_LEVEL flRight = Adapter::GetSupportedFeatureLevel(right);
	if (flLeft != flRight) {
		if (flLeft >= flRight) {
			return true;
		}
		return false;
	}
	else {
		SIZE_T ssml = descLeft.SharedSystemMemory;
		SIZE_T ssmr = descRight.SharedSystemMemory;
		if (ssml == ssmr) { return Adapter::getAdapterIndex(left) > Adapter::getAdapterIndex(right);; }
		if (ssml >= ssmr) { return true; }
		return false;
	}
}

int Adapter::getAdapterIndex(IDXGIAdapter* adapter) {
	for (SIZE_T i = 0; i < _adapters.size(); i++)
	{
		if (_adapters[i] == adapter)
		{
			return (int)i;
		}
	}
	return -1;	//Data leak, shoudn't happen
}