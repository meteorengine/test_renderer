#pragma once
enum RendererType		// Renderer type
{
	Auto,				// Let the game pick automagically best renderer 
	DirectX_10 = 100,	// Use DirectX 10
	DirectX_10_1,		// Use DirectX 10.1
	DirectX_11 = 110	// Use DirectX 11
};