#pragma once
class IGPUResource
{
public:
	virtual int MemoryUsage() = 0;
	virtual bool IsFullyLoaded() = 0;
};

