#pragma once
class GPUResource :
	public IGPUResource
{
private:
	std::string _name;
public:
	GraphicsDevice* Device;
	std::string Name();
	virtual int MemoryUsage() = 0;
	virtual bool IsFullyLoaded() = 0;
	virtual ~GPUResource();
protected:
	GPUResource(GraphicsDevice* device, std::string name);
};

